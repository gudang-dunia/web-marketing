#!/bin/sh
docker build -t "web-backoffice" . || true
docker rm $(docker stop $(docker ps -a -q --filter name="web-backoffice" --format="{{.ID}}")) || true
docker run -i -d -p 80:80 --name="web-backoffice" -v $PWD:/app "web-backoffice"
