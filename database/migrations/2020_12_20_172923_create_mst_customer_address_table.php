<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstCustomerAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_customer_address', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('company_id',100)->nullable();
            $table->string('customer_id',100)->nullable();
            $table->text('address')->nullable();
            $table->string('address_no',225)->nullable();
            $table->string('rt',225)->nullable();
            $table->string('rw',225)->nullable();
            $table->string('village',225)->nullable();
            $table->string('sub_district',225)->nullable();
            $table->string('benchmark',225)->nullable();
            $table->string('district',225)->nullable();
            $table->string('city_id',225)->nullable();
            $table->string('province_id',225)->nullable();
            $table->string('postal_code',225)->nullable();
            $table->string('author',100)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('created_by',100)->nullable();
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
            $table->index('id');
            $table->index('company_id');
            $table->index('customer_id');
            $table->index('district');
            $table->index('city_id');
            $table->index('province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_customer_address');
    }
}
