<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstCommissionRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_commission_rate', function (Blueprint $table) {
          $table->string('id')->primary();
          $table->string('company_id')->nullable();
          $table->string('sales_id')->nullable();
          $table->dateTime('start_date')->nullable();
          $table->dateTime('end_date')->nullable();
          $table->decimal('commission_price', $precision = 16, $scale = 2)->nullable()->default(10000);
          $table->string('author')->nullable();
          $table->string('status')->nullable();
          $table->string('created_by')->nullable();
          $table->string('updated_by')->nullable();
          $table->timestamps();
          $table->index('id');
          $table->index('company_id');
          $table->index('sales_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_commission_rate');
    }
}
