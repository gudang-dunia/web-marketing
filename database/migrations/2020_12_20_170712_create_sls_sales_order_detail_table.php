<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlsSalesOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sls_sales_order_detail', function (Blueprint $table) {
          $table->string('id')->primary();
          $table->string('company_id')->nullable();
          $table->string('sales_order_id')->nullable();
          $table->string('voucher_id')->nullable();
          $table->string('item_id')->nullable();
          $table->integer('quantity')->nullable()->default(0);
          $table->decimal('price', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('voucer', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('total_transaction', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('commission_price', $precision = 16, $scale = 2)->nullable()->default(10000);
          $table->string('author')->nullable();
          $table->string('status')->nullable();
          $table->string('sales_id')->nullable();
          $table->string('created_by')->nullable();
          $table->string('updated_by')->nullable();
          $table->timestamps();
          $table->index('id');
          $table->index('company_id');
          $table->index('sales_order_id');
          $table->index('sales_id');
          $table->index('item_id');
          $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sls_sales_order_detail');
    }
}
