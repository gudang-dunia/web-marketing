<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlsProductVisitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sls_product_visitor', function (Blueprint $table) {
          $table->string('id')->primary();
          $table->string('company_id')->nullable();
          $table->string('shift_work_id')->nullable();
          $table->string('item_id')->nullable();
          $table->string('advertise_id')->nullable();
          $table->string('interaction_id')->nullable();
          $table->string('customer_id')->nullable();
          $table->string('customer_address_id')->nullable();
          $table->integer('quantity')->nullable()->default(0);
          $table->decimal('price', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('courier_cost', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('insurance', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('cost_handler', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('voucher_total', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('transaction', $precision = 16, $scale = 2)->nullable()->default(0);
          $table->decimal('commission_price', $precision = 16, $scale = 2)->nullable()->default(10000);
          $table->string('voucher_id')->nullable();
          $table->string('market_id')->nullable();
          $table->string('courier_id')->nullable();
          $table->string('payment_type_id')->nullable();
          $table->string('bank_id')->nullable();
          $table->dateTime('transaction_date')->nullable();
          $table->dateTime('delivery_date')->nullable();
          $table->string('delivery_no')->nullable();
          $table->string('delivery_refno')->nullable();
          $table->text('delivery_remark')->nullable();
          $table->string('confirm_status')->nullable();
          $table->string('confirm_by')->nullable();
          $table->dateTime('confirm_date')->nullable();
          $table->tinyInteger('closing_status')->nullable()->default(0);
          $table->string('closing_by')->nullable();
          $table->dateTime('closing_date')->nullable();
          $table->string('packing_id')->nullable();
          $table->dateTime('packing_date')->nullable();
          $table->text('img_transaction')->nullable();
          $table->text('img_packing')->nullable();
          $table->text('img_delivery')->nullable();
          $table->tinyInteger('lock_status')->nullable()->default(0);
          $table->tinyInteger('followup_status')->nullable()->default(0);
          $table->string('sales_id')->nullable();
          $table->string('author')->nullable();
          $table->string('status')->nullable();
          $table->string('created_by')->nullable();
          $table->string('updated_by')->nullable();
          $table->timestamps();
          $table->index('id');
          $table->index('company_id');
          $table->index('shift_work_id');
          $table->index('item_id');
          $table->index('advertise_id');
          $table->index('interaction_id');
          $table->index('customer_id');
          $table->index('customer_address_id');
          $table->index('voucher_id');
          $table->index('market_id');
          $table->index('courier_id');
          $table->index('payment_type_id');
          $table->index('bank_id');
          $table->index('packing_id');
          $table->index('delivery_no');
          $table->index('delivery_refno');
          $table->index('sales_id');
          $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sls_product_visitor');
    }
}
