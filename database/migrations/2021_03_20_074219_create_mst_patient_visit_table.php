<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstPatientVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_patient_visit', function (Blueprint $table) {
              $table->string('id',50)->primary();
              $table->string('company_id',100)->nullable();
              $table->string('img_url')->nullable();
              $table->string('gender_id')->nullable();
              $table->string('name');
              $table->dateTime('birthdate',0)->nullable();
              $table->string('phone')->nullable();
              $table->string('email')->nullable();
              $table->string('consultation')->nullable();
              $table->dateTime('consultation_date')->nullable();
              $table->string('city_id',100)->nullable();
              $table->text('address')->nullable();
              $table->string('sales_id',100)->nullable();
              $table->string('register_status',100)->nullable();
              $table->tinyInteger('is_consultation')->default(0);
              $table->tinyInteger('is_order')->default(0);
              $table->tinyInteger('is_therapy')->default(0);
              $table->text('note')->nullable();
              $table->string('created_by',100)->nullable();
              $table->string('updated_by',100)->nullable();
              $table->timestamps();
              $table->index('id');
              $table->index('company_id');
              $table->index('gender_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_patient_visit');
    }
}
