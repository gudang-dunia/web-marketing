@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    {{$module}}
                </h5>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-xxl-4">
        <div class="card card-custom body-container">
          <div class="card-body rounded p-0 d-flex bg-light">
            <div class="col-lg-12 py-5 py-md-1 px-1 px-md-5 pr-lg-0">
              <div class="font-size-h4 mb-0">Show data current date</div>
              <form class="col-lg-11 d-flex flex-center py-2 px-6 bg-white rounded">
                <div hidden><input id="dashboard_startdate" name="dashboard_startdate" value="{{$dashboard_startdate}}"/><input id="dashboard_enddate" name="dashboard_enddate" value="{{$dashboard_enddate}}"/></div>
                <input type="text" class="form-control border-0 font-weight-bold pl-2 text-center" placeholder="Search Goods" id="dashboard_date" name="dashboard_date" value="{{$dashboard_date}}" readonly/>
                <!-- <div class="col-lg-4"> -->
                    <select class="form-control select2" id="company_id" name="company_id" style="width: 100%;">
                      @php $data = list_model('Master','Company') @endphp
                      @isset ($data)
                        @foreach($data as $rs)
                          @if($rs->id == sess_user('company_id'))
                            <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                          @else
                            <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                          @endif
                        @endforeach
                      @endisset
                   </select>
                <!-- </div> -->
                <input type="submit" class="btn btn-success btn-sm" value="show">
              </form>
            </div>
            <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-xxl-4">
        <div class="row col-12 mx-1">
    			<div class="col-lg-3 col-sm-3 col-3">
    				<div class="card card-custom bg-success card-stretch gutter-b">
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$dash1['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">{{$dash1['name']}}</span>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<div class="card card-custom bg-warning card-stretch gutter-b">
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$dash2['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">{{$dash2['name']}}</span>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<div class="card card-custom bg-info card-stretch gutter-b">
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$dash3['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">{{$dash3['name']}}</span>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<div class="card card-custom bg-danger card-stretch gutter-b">
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 text-hover-primary d-block">{{$dash4['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">{{$dash4['name']}}</span>
    					</div>
    				</div>
    			</div>
    		</div>
  		</div>
		</div>
    <div class="row">
      <div class="col-lg-6">
        <div class="col-lg-12">
          <div class="row d-flex flex-center">
            @foreach(list_confirm_status() as $crfm)
              @if (!$dash12->isEmpty())
                @foreach($dash12 as $data)
                  @if ($crfm == $data->status)
                		<div class="col-xl-4">
                			<div class="card card-custom bgi-no-repeat card-stretch gutter-b mb-2 mt-2">
                				<div class="card-body pb-2 pt-2">
                					<span class="card-title font-weight-bolder text-dark font-size-h2 mb-0 mt-0 d-block">{{$data->jml}} <i class="fa icon-xl text-primary flaticon-pie-chart-1"></i></span>
                          <span class="font-weight-bolder text-info font-size-h4">{{$data->status}}</span>
                				</div>
                			</div>
                		</div>
                  @endif
                @endforeach
              @else
                <div class="col-xl-4">
                  <div class="card card-custom bgi-no-repeat card-stretch gutter-b mb-2 mt-2">
                    <div class="card-body pb-2 pt-2">
                      <span class="card-title font-weight-bolder text-dark font-size-h2 mb-0 mt-0 d-block">0 <i class="fa icon-xl text-primary flaticon-pie-chart-1"></i></span>
                      <span class="font-weight-bolder text-info font-size-h4">{{$crfm[1]}}</span>
                    </div>
                  </div>
                </div>
              @endif
            @endforeach
        	</div>
        </div>
        @if($dash8)
        <div class="col-lg-12">
					<div class="card card-custom gutter-b mb-2 mt-2">
						<div class="card-body d-flex flex-column">
							<div class="d-flex align-items-center justify-content-between flex-grow-1">
								<div class="mr-2">
									<h3 class="font-weight-bolder">Bank Received</h3>
									<div class="text-muted font-size-lg mt-1">CRM Report for sales order period</div>
								</div>
								<div class="font-weight-boldest font-size-h1 text-warning">Rp. {{number_format($dash8->total,2)}}</div>
							</div>
						</div>
					</div>
        </div>
        @endif
        @if($dash11)
        <div class="col-lg-12">
					<div class="card card-custom gutter-b mb-2 mt-2">
						<div class="card-body d-flex flex-column">
							<div class="d-flex align-items-center justify-content-between flex-grow-1">
								<div class="mr-2">
									<h3 class="font-weight-bolder">Packing Commision</h3>
                  <div class="text-muted font-size-lg mt-1">Total Packing SO period {{$dash11->jml}} Packet</div>
								</div>
								<div class="font-weight-boldest font-size-h1 text-warning">Rp. {{number_format($dash11->packing_commission,2)}}</div>
							</div>
						</div>
					</div>
        </div>
        @endif
        <div class="col-lg-12">
          <div class="row d-flex flex-center">
            @if($dash5)
              @foreach($dash5 as $data)
            		<div class="col-xl-4">
            			<div class="card card-custom bgi-no-repeat card-stretch gutter-b mb-2 mt-2">
            				<div class="card-body pb-2 pt-2">
            					<span class="card-title font-weight-bolder text-dark font-size-h2 mb-0 mt-0 d-block">{{$data->jml}} <i class="fa icon-xl text-primary flaticon-pie-chart-1"></i></span>
                      <span class="font-weight-bolder text-info font-size-h4">{{$data->status}}</span>
            				</div>
            			</div>
            		</div>
              @endforeach
            @endif
        	</div>
        </div>
        <div class="col-lg-12">
          <div class="col-xl-12">
						<div class="card card-custom card-stretch gutter-b">
							<div class="card-header border-0">
								<h3 class="card-title font-weight-bolder text-dark">Marketing</h3>
								<div class="card-toolbar">
									<div class="dropdown dropdown-inline">
									</div>
								</div>
							</div>
							<div class="card-body pt-2">
                @if($dash7)
                  @foreach($dash7 as $data)
      							<div class="d-flex align-items-center mb-10">
      								<div class="symbol symbol-40 symbol-light-success mr-5">
      									<span class="symbol-label">
      										<img src="{{asset('/media/svg/avatars/009-boy-4.svg')}}" class="h-75 align-self-end" alt="">
      									</span>
      								</div>
      								<div class="d-flex flex-column flex-grow-1 font-weight-bold">
      									<a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{{$data->author}}</a>
                          @php $author = $data->author @endphp
                          <span class="text-bold">
                            @foreach($dash6 as $dt)
                              @if($dt->author == $author)
                                {{$dt->status}}[{{$dt->jml}}]
                              @endif
                            @endforeach
                          </span>
      								</div>
      							</div>
                  @endforeach
                @endif
							</div>
						</div>
					</div>
        </div>
      </div>
      <div class="col-lg-6">
          @if($dash9)
          <div class="col-lg-12">
    				<div class="card card-custom gutter-b mb-2 mt-2">
    					<div class="card-body d-flex flex-column">
    						<div class="d-flex align-items-center justify-content-between flex-grow-1">
    							<div class="mr-2">
    								<h3 class="font-weight-bolder">Success Income</h3>
                    <div class="text-muted font-size-lg mt-1">Total sales order period {{$dash9->quantity}} Botol</div>
    							</div>
    							<div class="font-weight-boldest font-size-h1 text-success">Rp. {{number_format($dash9->quantity*$dash9->commission_price,2)}}</div>
    						</div>
    					</div>
    				</div>
          </div>
          @endif
          @if($dash10)
          <div class="col-lg-12">
    				<div class="card card-custom gutter-b mb-2 mt-2">
    					<div class="card-body d-flex flex-column">
    						<div class="d-flex align-items-center justify-content-between flex-grow-1">
    							<div class="mr-2">
    								<h3 class="font-weight-bolder">On Progress Income</h3>
                    <div class="text-muted font-size-lg mt-1">Total sales order period {{$dash10->quantity}} Botol</div>
    							</div>
    							<div class="font-weight-boldest font-size-h1 text-warning">Rp. {{number_format($dash10->quantity*$dash10->commission_price,2)}}</div>
    						</div>
    					</div>
    				</div>
          </div>
          @endif
          <div class="col-lg-12">
            <div class="card card-custom {{ @$class }}">
                <div class="card-header align-items-center border-0 mt-4">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="font-weight-bolder text-dark">Tracking Period</span>
                    </h3>
                </div>
                <div class="card-body pt-4">
                    <div class="timeline timeline-5 mt-3">
                        @if($dash_track)
                          @foreach($dash_track as $data)
                            <div class="timeline-item align-items-start">
                              <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-1">{{date('d m y H:i', strtotime($data->updated_at))}}</div>
                              <div class="timeline-badge"><i class="fa fa-genderless @if($data->followup_status) text-success @else text-danger @endif icon-xxl"></i></div>
                              <div class="timeline-content d-flex flex-column">
                                  <span class="mr-4 font-weight-bolder text-dark-75">{{$data->id}}-[{{$data->confirm_status}}]</span>
                                  <div class="timeline-content text-dark-50">{{$data->gender_name}} {{$data->full_name}}</div>
                                  <div class="timeline-content text-dark-50">Resi: {{$data->delivery_no}}</div>
                                  <div class="timeline-content text-dark-50">{{$data->quantity}} Botol</div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                        <div class="timeline-item align-items-start">
                            <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('D')}}</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary icon-xxl"></i>
                            </div>
                            <div class="timeline-content text-dark-50">
                                Tracking Period.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="col-lg-12 mt-2">
            <div class="card card-custom {{ @$class }}">
                <div class="card-header align-items-center border-0 mt-4">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="font-weight-bolder text-dark">Aktivitas Period</span>
                    </h3>
                </div>
                <div class="card-body pt-4">
                    <div class="timeline timeline-5 mt-3">
                        @if($dash)
                          @foreach($dash as $data)
                            <div class="timeline-item align-items-start">
                              <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-1">{{date('d m y H:i', strtotime($data->created_at))}}</div>
                              <div class="timeline-badge"><i class="fa fa-genderless @if($data->followup_status) text-success @else text-danger @endif icon-xxl"></i></div>
                              <div class="timeline-content d-flex flex-column">
                                  <span class="mr-4 font-weight-bolder text-dark-75">{{$data->id}}-[{{$data->confirm_status}}]</span>
                                  <div class="timeline-content text-dark-50">Marketing: {{$data->author}}</div>
                                  <div class="timeline-content text-dark-50">{{$data->gender_name}} {{$data->full_name}}</div>
                                  <div class="timeline-content text-dark-50">Phone: {{$data->phone}}</div>
                                  <div class="timeline-content text-dark-50">Jml: {{$data->quantity}} Btl</div>
                                  <div class="timeline-content text-dark-50">Transaksi: Rp. {{number_format($data->transaction,2)}}</div>
                                  <div class="timeline-content text-dark-50">Update By: {{$data->updated_by}}</div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                        <div class="timeline-item align-items-start">
                            <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('D')}}</div>
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-primary icon-xxl"></i>
                            </div>
                            <div class="timeline-content text-dark-50">
                                Aktivitas Kerja Hari ini.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </div>

@endsection


{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
      $('.select2').select2();
      $('#dashboard_date').daterangepicker({
          format: 'DD-MM-YYYY hh:mm',
          startDate: "{{$dashboard_datetime}}".split(' <=> ')[0],
          endDate: "{{$dashboard_datetime}}".split(' <=> ')[1],
          opens: "center",
          drops: "auto",
          timePicker: true,
          timePicker24Hour: true,
          autoUpdateInput: false
       });
      $('#dashboard_date').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD-MM-YYYY hh:mm') + ' <=> ' + picker.endDate.format('DD-MM-YYYY hh:mm'));
         $("#dashboard_startdate").val(picker.startDate.format('YYYY-MM-DD hh:mm'));
         $("#dashboard_enddate").val(picker.endDate.format('YYYY-MM-DD hh:mm'));
      });

      $('#dashboard_date').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $("#dashboard_startdate").val('');
        $("#dashboard_enddate").val('');
      });
    });
</script>
@endsection
