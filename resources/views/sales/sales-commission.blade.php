@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module_alias }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('product.index','interaksi') }}" class="text-muted">{{ $module_alias }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="container pb-2 body-container">
  <div class="col-lg-12">
    <div class="card card-custom body-container">
      <div class="col-lg-12">
        <h1 class="font-weight-bolder text-dark mb-0">Search:</h1>
      </div>
      <div class="card-body rounded p-1 bg-light">
        <div class=" d-flex flex-wrap justify-content-center">
          <div class="col-lg-4">
              <select class="form-control select2" id="company_id" name="company_id" style="width: 100%;">
                @php $data = list_model('Master','Company') @endphp
                @isset ($data)
                  @foreach($data as $rs)
                    @if($rs->id == sess_user('company_id'))
                      <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                    @else
                      <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                    @endif
                  @endforeach
                @endisset
             </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="advertise_id" name="advertise_id" style="width: 100%;">
              <option value="" selected>Chose Advertise</option>
               @php $data = list_model('Master','Ads') @endphp
               @isset ($data)
                 @foreach($data as $rs)
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                 @endforeach
               @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="bank_id" name="bank_id" style="width: 100%;">
              <option value="" selected>Chose Bank</option>
               @php $data = list_model('Master','Bank') @endphp
               @isset ($data)
                 @foreach($data as $rs)
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                 @endforeach
               @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="transaction_status" name="transaction_status" style="width: 100%;">
              <option value="" selected>Chose Transaction</option>
              @php $data = list_confirm_status() @endphp
              @isset ($data)
                @foreach($data as $rs)
                 <option value="{{ $rs[0] }}">{{ $rs[1] }}</option>
                @endforeach
              @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="courier_id" name="courier_id" style="width: 100%;">
              <option value="" selected>Chose Courier</option>
               @php $data = list_model('Master','Courier') @endphp
               @isset ($data)
                 @foreach($data as $rs)
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                 @endforeach
               @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="resi_status" name="resi_status" style="width: 100%;">
              <option value="" selected>Status Resi</option>
              <option value="off">Resi Empty</option>
              <option value="on" >Resi Exist</option>
            </select>
          </div>
        </div>
        <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
      </div>
    </div>
  </div>
</div>
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module_alias }}
        <!-- <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module_alias }}</div> -->
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0 col-lg-4 d-flex">
      <div class="col-lg-8">
          <select class="form-control select2 closing_period_id" id="closing_period_id" name="closing_period_id" style="width: 100%;">
            <option value="" selected>Chose Period</option>
            @php $data = list_model('Master','ClosingPeriod') @endphp
            @isset ($data)
              @foreach($data as $rs)
               <option value="{{ $rs->id }}">{{ $rs->name }}</option>
              @endforeach
            @endisset
         </select>
      </div>
      <div class="col-lg-4">
        <div class="dropdown dropdown-inline px-2">
            <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-download text-white"></i> Tools
            </button>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul class="navi flex-column navi-hover py-2" id="btn_tools">
                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                        Export Tools
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_marketing_global()">
                            <span class="navi-icon"><i class="la la-print"></i></span>
                            <span class="navi-text">Print Marketing Global</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_marketing_detail()">
                            <span class="navi-icon"><i class="la la-print"></i></span>
                            <span class="navi-text">Print Marketing Detail</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_packing_global()">
                            <span class="navi-icon"><i class="la la-print"></i></span>
                            <span class="navi-text">Print Packing Global</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_packing_detail()">
                            <span class="navi-icon"><i class="la la-print"></i></span>
                            <span class="navi-text">Print Packing Detail</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <div hidden><input id="dashboard_startdate" name="dashboard_startdate" value="{{$dashboard_startdate}}"/><input id="dashboard_enddate" name="dashboard_enddate" value="{{$dashboard_enddate}}"/></div>
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
     <div class="card-body pt-0 datatable_detail">
       <div class="d-flex col-md-12">
  				<div class="col-md-12">
            <div class="table-responsive">
              <div class="card-header bg-danger p-2 pb-0">
                <div class="card-title pb-0 mb-0">
                    <h3 class="text-white pb-0 mb-0" style="font-weight:900"><div id="color-picker-1" class="mx-auto">Sales Order Commission Rate <label class="module_name"></label></div></h3>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table id="datatable_detail" class="datatable_detail table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0">
                  <thead>
                    <tr>
                        <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Transaction</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Jml Packet</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Bank</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
                        <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Commission</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
            </div>
  				</div>
       </div>
     </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $(".is_reqs").hide();
    $('.select2').select2({});
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end").change(function(){
      refresh_table();
    });
    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  $('[data-switch=true]').bootstrapSwitch('state', true);
  $('#status').on('switchChange.bootstrapSwitch', function (event, state) {
      var x = $(this).data('on-text');
      var y = $(this).data('off-text');
      if ($("#status").is(':checked')) {
          $(".is_reqs").show(500);
      } else {
          $(".is_reqs").hide(500);
      }
  });


  $('.datetimepicker-input').datetimepicker({
      format: 'dd-mm-yyyy hh:ii'
  });
  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: false,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
        d.advertise_id = $("#advertise_id").val();
        d.bank_id = $("#bank_id").val();
        d.transaction_status = $("#transaction_status").val();
        d.resi_status = $("#resi_status").val();
        d.courier_id = $("#courier_id").val();
        d.company_id = $("#company_id").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "Marketing ID", data: 'sales_id', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Marketing", data: 'author', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Total Komisi", data: 'sum_commission', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Quantity", data: 'sum_quantity', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Transaction", data: 'sum_price', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Delivery Cost", data: 'sum_courier', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Handler Cost", data: 'sum_cost_handler', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
      {title: "Total Transaction", data: 'sum_transaction', defaultContent: '-', class: 'text-center dt-body-nowrap', searchable: false},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

    $('#datatable tbody').on( 'click', 'tr', function () {
      $("#datatable_so tbody>tr").remove();
      if ($(this).hasClass('selected')) {
        table.$('tr.selected').removeClass('selected');
        $(this).removeClass('selected');
        $(".module_name").text('-');
        $("#datatable_detail tbody>tr").remove();
      } else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var id = $(this).find("td:eq(1)").html();
        $(".module_name").text(" : "+$(this).find("td:eq(1)").html());
        list_data(id);
      }
    });

    function list_data(id = "") {
        $(".datatable_detail").loading("start");
        $("#datatable_detail tbody>tr").remove();
        if (id !== "") {
          $.ajax({
              url : '{{ $path }}/list',
              type: "POST",
              data: {
                detail : 1,
                company_id : $("#company_id").val(),
                sales_id:id,
                from_date:formatdate($("#datesearch_start").val()),
                to_date:formatdate($(" #datesearch_end").val(),true)
              },
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                  for (i in response.data){
                    var rs = response.data[i];
                    var table = document.getElementById("datatable_detail").getElementsByTagName('tbody')[0];
                    var row = table.insertRow(-1);
                    var closing_header = "'"+rs.id+"'";
                    var voucher = parseFloat(rs.voucher_total);
                    var quantity = parseFloat(rs.quantity);
                    var cost_handler = parseFloat(rs.cost_handler);
                    var price = parseFloat(rs.price);
                    var total_product = ((quantity*price)-voucher);
                    var status = '<a class="btn btn-outline-warning"><i class="flaticon2-poll-symbol"></i>Rp. '+parseFloat(rs.sum_commission)+'</a>';
                    if (rs.closing_status){
                      status = '<a class="btn btn-outline-danger"><i class="flaticon2-poll-symbol"></i>Rp. '+parseFloat(rs.sum_commission)+'</a>';
                    }
                      row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                      row.insertCell(1).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.id+'</label>';
                      row.insertCell(2).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_no+'</label>';
                      row.insertCell(3).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+formatdate(rs.transaction_date)+'</label>';
                      row.insertCell(4).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.gender_name+' '+rs.full_name+'</label>';
                      row.insertCell(5).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.quantity+'<br>'+rs.courier_name+'</label>';
                      row.insertCell(6).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.bank_name+'<br>Rp.'+addCommas(total_product)+'</label>';
                      row.insertCell(7).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.author+'</label>';
                      row.insertCell(8).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.confirm_status+'<br>'+status+'</label>';
                  }
                  $(".datatable_detail").loading("stop");
                }
              },
              error: function (xhr, status, error) {
                $(".datatable_detail").loading("stop");
              }
          });
        } else {

        }
    }


  function export_print(){
    var from_date = $("#dashboard_startdate").val();
    var to_date = $(" #dashboard_enddate").val();
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
    window.open("{{ $path_export }}?from_date="+from_date+"&to_date="+to_date);
  }

  function export_excel(){
    var from_date = $("#dashboard_startdate").val();
    var to_date = $(" #dashboard_enddate").val();
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
  }


  $("#company_id").change(function(){
    $('#advertise_id').empty();
    $('#advertise_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','ads') }}/list?company_id=" + $("#company_id").val(),
        type: "GET",
        success: function (response) {
          $('#advertise_id').empty();
          $('#advertise_id').append('<option value="">Chose Advertise</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#advertise_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    $('#bank_id').empty();
    $('#bank_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','bank') }}/list?company_id=" + $("#company_id").val(),
        type: "GET",
        success: function (response) {
          $('#bank_id').empty();
          $('#bank_id').append('<option value="">Chose Bank</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#bank_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    $('#courier_id').empty();
    $('#courier_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','courier') }}/list?company_id=" + $("#company_id").val(),
        type: "GET",
        success: function (response) {
          $('#courier_id').empty();
          $('#courier_id').append('<option value="">Chose Courier</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#courier_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    $('#closing_period_id').empty();
    $('#closing_period_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','closing-period') }}/list?company_id=" + $("#company_id").val(),
        type: "GET",
        success: function (response) {
          $('#closing_period_id').empty();
          $('#closing_period_id').append('<option value="">Chose Period</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#closing_period_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    refresh_table();
  });

  $("#bank_id, #transaction_status, #advertise_id, #resi_status, #courier_id").change(function(){
    refresh_table();
  });

  function export_marketing_global() {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
    var transaction_status = $("#transaction_status").val();
    if( transaction_status !=""){
      transaction_status = "&transaction_status="+$("#transaction_status").val();
    }
    var closing_period_id = $("#closing_period_id").val();
    if(closing_period_id != ""){
      closing_period_id = "&closing_period_id=" + $("#closing_period_id option:selected" ).val();
      closing_period_id += "&closing_period=" + $("#closing_period_id option:selected" ).text();
    }
    window.open("{{ ENV('API_RPT_URL') }}/sales-commission.php?company_id="+$("#company_id").val()+"&from_date="+from_date+"&to_date="+to_date+transaction_status+closing_period_id);
  }

  function export_marketing_detail() {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }

    var transaction_status = "";
    if($("#transaction_status").val() !=""){
      transaction_status = "&transaction_status="+$("#transaction_status").val();
    }
    var closing_period_id = $("#closing_period_id").val();
    if(closing_period_id != ""){
      closing_period_id = "&closing_period_id=" + $("#closing_period_id option:selected" ).val();
      closing_period_id += "&closing_period=" + $("#closing_period_id option:selected" ).text();
    }
    window.open("{{ ENV('API_RPT_URL') }}/sales-commission-detail.php?company_id="+$("#company_id").val()+"&from_date="+from_date+"&to_date="+to_date+transaction_status+closing_period_id);
  }

  function export_packing_global() {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
    var closing_period_id = $("#closing_period_id").val();
    if(closing_period_id != ""){
      closing_period_id = "&closing_period_id=" + $("#closing_period_id option:selected" ).val();
      closing_period_id += "&closing_period=" + $("#closing_period_id option:selected" ).text();
    }
    var transaction_status = "";
    if($("#transaction_status").val() !=""){
      transaction_status = "&transaction_status="+$("#transaction_status").val();
    }
    window.open("{{ ENV('API_RPT_URL') }}/packing-commission.php?company_id="+$("#company_id").val()+"&from_date="+from_date+"&to_date="+to_date+transaction_status+closing_period_id);
  }

  function export_packing_detail() {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
    var closing_period_id = $("#closing_period_id").val();
    if(closing_period_id != ""){
      closing_period_id = "&closing_period_id=" + $("#closing_period_id option:selected" ).val();
      closing_period_id += "&closing_period=" + $("#closing_period_id option:selected" ).text();
    }

    var transaction_status = "";
    if($("#transaction_status").val() !=""){
      transaction_status = "&transaction_status="+$("#transaction_status").val();
    }
    window.open("{{ ENV('API_RPT_URL') }}/packing-commission-detail.php?company_id="+$("#company_id").val()+"&from_date="+from_date+"&to_date="+to_date+transaction_status+closing_period_id);
  }

  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }
</script>
@endsection
