@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module_alias }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $path }}" class="text-muted">{{ $module_alias }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')

<div class="container pb-2 body-container">
  <div class="col-lg-12">
    <div class="card card-custom body-container">
      <div class="col-lg-12">
        <h1 class="font-weight-bolder text-dark mb-0">Search:</h1>
      </div>
      <div class="card-body rounded p-1 bg-light">
        <div class=" d-flex flex-wrap justify-content-center">
          <div class="col-lg-4">
              <select class="form-control select2" id="company" name="company" style="width: 100%;">
                @php $data = list_model('Master','Company') @endphp
                @isset ($data)
                  @foreach($data as $rs)
                    @if($rs->id == sess_user('company_id'))
                      <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                    @else
                      <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                    @endif
                  @endforeach
                @endisset
             </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="advertise_id" name="advertise_id" style="width: 100%;">
              <option value="" selected>Chose Advertise</option>
               @php $data = list_model('Master','Ads') @endphp
               @isset ($data)
                 @foreach($data as $rs)
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                 @endforeach
               @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="payment_type_id" name="payment_type_id" style="width: 100%;">
              <option value="" selected>Chose Payment</option>
               @php $data = list_model('Master','PaymentType') @endphp
               @isset ($data)
                 @foreach($data as $rs)
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                 @endforeach
               @endisset
            </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="transaction_status" name="transaction_status" style="width: 100%;">
              <option value="" selected>Chose Transaction</option>
              @php $data = list_confirm_status() @endphp
              @isset ($data)
                @foreach($data as $rs)
                 <option value="{{ $rs[0] }}">{{ $rs[1] }}</option>
                @endforeach
              @endisset
            </select>
          </div>
          <div class="col-lg-4">
              <select class="form-control select2" id="courier_id" name="courier_id" style="width: 100%;">
                <option value="" selected>Chose Courier</option>
                 @php $data = list_model('Master','Courier') @endphp
                 @isset ($data)
                   @foreach($data as $rs)
                    <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                   @endforeach
                 @endisset
              </select>
          </div>
          <div class="col-lg-4">
            <select class="form-control select2" id="resi_status" name="resi_status" style="width: 100%;">
              <option value="" selected>Status Resi</option>
              <option value="off">Resi Empty</option>
              <option value="on" >Resi Exist</option>
            </select>
          </div>
        </div>
        <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
      </div>
    </div>
  </div>
</div>

<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module_alias }}
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0">
      <div class="col-lg-12">
        <a data-toggle="modal" data-target="#modal-query"  class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
          Import
        </a>
        <div class="dropdown dropdown-inline px-2">
            <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-download text-white"></i> Tools
            </button>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul class="navi flex-column navi-hover py-2" id="btn_tools">
                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                        Export Tools
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_do(1)">
                            <span class="navi-icon"><i class="la la-copy"></i></span>
                            <span class="navi-text">List Order by Resi</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_do(0)">
                            <span class="navi-icon"><i class="la la-copy"></i></span>
                            <span class="navi-text">List Order By E-Commerce</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_copy()">
                            <span class="navi-icon"><i class="la la-copy"></i></span>
                            <span class="navi-text">List DO By WA</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100 responsive" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>

   <div class="card-body pt-0 datatable_detail">
     <div class="col-md-12">
       <div class="d-flex flex-column-fluid">
  				<div class="container-fluid">
            <div class="table-responsive">
              <div class="card-header bg-danger p-2 pb-0">
                <div class="card-title pb-0 mb-0">
                    <h3 class="text-white pb-0 mb-0" style="font-weight:900"><div id="color-picker-1" class="mx-auto">Detail Transaksi <label class="madule_name"></label></div></h3>
                </div>
              </div>
            </div>
  					<div class="card card-custom overflow-hidden">
  						<div class="card-body invoice-6 p-0">
  							<div class="invoice-6-container bgi-size-contain bgi-no-repeat bgi-position-y-top bgi-position-x-center" style="background-image: url({{asset('media/svg/shapes/abstract-10.svg')}});">
  								<div class="container">
  									<div class="row justify-content-center py-8 px-2 py-md-2 px-md-0">
  										<div class="col-md-9">
  											<div class="d-flex justify-content-between align-items-center flex-column flex-md-row mb-5">
  												<h1 class="display-6 font-weight-boldest text-dark-light mb-5 mb-md-0 sales_order str_default">-</h1>
  												<div class="d-flex flex-column px-0 text-right">
  													<span class="d-flex flex-column font-size-h5 font-weight-bold text-dark-light align-items-center align-items-md-end">
  														<span class="mb-2 marketing_name str_default">-</span>
  														<span class="font-weight-boldest text-dark-light transaction_date str_default">-</span>
  													</span>
  												</div>
  											</div>
  											<div class="table-responsive">
                          <table id="datatable_detail" class="datatable_detail table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0">
                              <thead>
                                <tr>
                                    <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                                    <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Item</th>
                                    <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Quantity</th>
                                    <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Price</th>
                                    <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Diskon</th>
                                    <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Total</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
  											</div>
  											<div class="row d-flex">
  												<div class="col-md-6">
  													<div class="d-flex flex-column text-dark-light m-0 p-0 mb-md-0">
        											<div class="d-flex flex-wrap align-items-end m-0 p-0">
        												<div>
        													<div class="font-size-h6 font-weight-boldest m-0 p-0 note-color description str_default">-</div>
        												</div>
        											</div>
  													</div>
  												</div>
  												<div class="col-md-6">
  													<div class="table-responsive">
  														<table id="table-header" class="table text-md-right font-weight-boldest">
  															<tbody>
  																<tr>
  																	<td class="p-0 font-weight-boldest border-0 pl-0 w-50">Transaksi</td>
  																	<td class="p-0 align-middle text-primary font-size-h6 border-0 pt-0"><label class="transaction str_default">Rp 0</label></td>
  																</tr>
  																<tr>
  																	<td class="p-0 font-weight-boldest border-0 pl-0 w-50">Total Voucher</td>
  																	<td class="p-0 align-middle text-primary font-size-h6 border-0 pt-0"><label class="voucher str_default">Rp 0</label></td>
  																</tr>
  																<tr>
  																	<td class="p-0 font-weight-boldest border-0 pl-0 w-50">Biaya Pengiriman</td>
  																	<td class="p-0 align-middle text-primary font-size-h6 border-0 pt-0"><label class="courier_cost str_default">Rp 0</label></td>
  																</tr>
  																<tr>
  																	<td class="p-0 font-weight-boldest border-0 pl-0 w-50">Biaya Penanganan</td>
  																	<td class="p-0 align-middle text-primary font-size-h6 border-0 pt-0"><label class="cost_handler str_default">Rp 0</label></td>
  																</tr>
  																<tr>
  																	<td class="p-0 font-weight-boldest border-0 pl-0 w-50">GRAND TOTAL</td>
  																	<td class="p-0 align-middle text-primary font-size-h4 border-0"><label class="grand_total str_default">Rp 0</label></td>
  																</tr>
  															</tbody>
  														</table>
  													</div>
                            <label class="customer_address str_default">Rp 0</label>
  												</div>
  											</div>
  										</div>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
     </div>
   </div>
</div>



<!-- Modal-->
<div class="modal fade" id="modal-query" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Form {{ $module }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="container pb-3">
              <div class="col-lg-12">
                <div class="card card-custom body-container">
                  <div class="col-lg-12">
                    <h1 class="font-weight-bolder text-dark mb-0">Import Query:&emsp;<input type="text" id="format_query" class="col-2 form-control-sm"/></h1>
                  </div>
                  <div class="card-body rounded p-1 d-flex bg-light">
                    <div class="col-lg-12">
                      <div class="form-group row px-2">
                        <label class="col-lg-2 col-form-label font-weight-bold text-hover-info h5">
                            <a id="generade" class="btn btn-outline-success"><i class="flaticon2-poll-symbol"></i> Generade</a>
                        </label>
                        <div class="col-lg-10">
                            <textarea id="resi_query" class="form-control" rows="10"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
                  </div>
                </div>
              </div>

              <div class="card card-custom body-container">
                <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
                  <div class="card-title pt-1 pb-1">
                    <h3 class="card-label font-weight-bolder text-white">Show Data Sales Order Import
                    </h3>
                  </div>
                  <div class="card-toolbar pt-1 pb-0">
                  </div>
                </div>
                <div class="card-body pt-1">
                  <table class="table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0" id="datatable_result">
                    <thead>
                      <tr>
                          <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Ref</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Url Tracking</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Phone</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Confirm</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')

<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $(".is_reqs").hide();
    $('.select2').select2({});
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end").change(function(){
      refresh_table();
    });
    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  $('[data-switch=true]').bootstrapSwitch('state', true);
  $('#status').on('switchChange.bootstrapSwitch', function (event, state) {
      var x = $(this).data('on-text');
      var y = $(this).data('off-text');
      if ($("#status").is(':checked')) {
          $(".is_reqs").show(500);
      } else {
          $(".is_reqs").hide(500);
      }
  });


  $('#generade').click(function(){
    var format_query = $('#format_query').val();
    if(format_query.replaceAll(" ","") == ""){
      format_query = "=";
    }
    var query = $('#resi_query').val();
    var list = query.split('\n');
    var list_order = [];
    var list_resi = [];
    var list_refno = [];
    var list_tracking = [];
    for (i in list){
      list_order.push(list[i].split(format_query)[0]);
      list_resi.push(list[i].split(format_query)[1]);
      list_refno.push(list[i].split(format_query)[2]);
      list_tracking.push(list[i].split(format_query)[3]);
    }

    $("#datatable_result tbody>tr").remove();
    $.ajax({
        method: 'POST',
        url : '{{ $path }}/list',
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        data: {list_so:list_order},
        success: function (response) {
          if(response.data){
            for (x in list){
              for (i in response.data){
                var rs = response.data[i];
                if(rs.id == list_order[x]){
                  var table = document.getElementById("datatable_result").getElementsByTagName('tbody')[0];
                  var row = table.insertRow(-1);
                  var order = "'"+rs.id+"'";
                  var resi = "'"+list_resi[x]+"'";
                  var refno = "'"+list_refno[x]+"'";
                  var tracking = "'"+list_tracking[x]+"'";
                  row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                  row.insertCell(1).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.id+'</label>';
                  row.insertCell(2).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+list_resi[x]+'</label>';
                  row.insertCell(3).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+list_refno[x]+'</label>';
                  row.insertCell(4).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+list_tracking[x]+'</label>';
                  row.insertCell(5).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.full_name+'</label>';
                  row.insertCell(6).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.phone+'</label>';
                  row.insertCell(7).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.author+'</label>';
                  if(rs.closing_status){
                    row.insertCell(8).innerHTML = '<a class="btn btn-outline-danger"><i class="flaticon2-poll-symbol"></i>closed</a>';
                  }else{
                    if(rs.confirm_status != '{{list_confirm_status()[0][0]}}' && rs.confirm_status != '{{list_confirm_status()[1][0]}}' && rs.confirm_status != '{{list_confirm_status()[2][0]}}'){
                      row.insertCell(8).innerHTML = '<a class="btn btn-outline-warning"><i class="flaticon2-poll-symbol"></i>'+rs.confirm_status+'</a>';
                    }else{
                      row.insertCell(8).innerHTML = '<a onclick="update_data(this,'+order+','+resi+','+refno+','+tracking+')" class="btn btn-outline-success"><i class="flaticon2-poll-symbol"></i>process</a>';
                    }
                  }
                }
              }
            }
          }

        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  });

  function update_data(btn,order,resi,refno,tracking){
    $("#modal-query").loading("start");
    $.ajax({
        url: "{{ $path }}/update/"+order+"?resi="+resi+"&refno="+refno+"&tracking="+tracking,
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          var row = btn.parentNode.parentNode;
          row.parentNode.removeChild(row);
          refresh_table();
          $("#modal-query").loading("stop");
        },
        error: function (xhr, status, error) {
            $("#modal-query").loading("stop");
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
        d.advertise_id = $("#advertise_id").val();
        d.transaction_status = $("#transaction_status").val();
        d.courier_id = $("#courier_id").val();
        d.payment_type_id = $("#payment_type_id").val();
        d.company_id = $("#company").val();
        d.resi_status = $("#resi_status").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "No Order", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "No Resi", data: 'delivery_no', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Cust ID", data: 'customer_id', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Advertise", data: 'advertise_name', name: 'mst_advertise.name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Cust Name", data: 'full_name', name: 'mst_customer.full_name', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap',searchable: false,  autoHide: true},
      {title: "Marketing", data: 'author', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: true},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });


  $('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_detail tbody>tr").remove();
    if ($(this).hasClass('selected')) {
        table.$('tr.selected').removeClass('selected');
        $(this).removeClass('selected');
        $(".str_default").text('-');
    } else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var id = $(this).find("td:eq(1)").html();
        $(".madule_name").text(" : "+$(this).find("td:eq(1)").html()+"[RESI:"+$(this).find("td:eq(2)").html()+"]");
        $(".datatable_detail").loading("start");
        $.ajax({
            url: "{{ $path }}/detail",
            type: "POST",
            data: {sales_order_id:$(this).find("td:eq(1)").html()},
            headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function (response) {
              if(response.data){
                  $("#datatable_detail tbody>tr").remove();
                  var table = document.getElementById("datatable_detail").getElementsByTagName('tbody')[0];
                  var price = 0;
                  var voucher = 0;
                  for (var i in response.data['detail']){
                    var rs = response.data['detail'][i];
                    voucher = voucher+parseFloat(rs.voucer);
                    price = price+parseFloat(rs.total_transaction);
                    var row = table.insertRow(-1);
                    row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                    row.insertCell(1).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+rs.item_name+'</label>';
                    row.insertCell(2).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+rs.quantity+'</label>';
                    row.insertCell(3).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+"Rp. "+parseFloat(rs.price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</label>';
                    row.insertCell(4).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+"Rp. "+parseFloat(rs.voucer).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</label>';
                    row.insertCell(5).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+"Rp. "+parseFloat(rs.total_transaction).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</label>';
                  }
                  var courier_cost = parseFloat(response.data['header'].courier_cost);
                  var cost_handler = parseFloat(response.data['header'].cost_handler);
                  var insurance = parseFloat(response.data['header'].insurance);
                  var total = price+courier_cost+insurance;

                  $(".transaction").text("Rp. "+addCommas(price));
                  $(".voucher").text("Rp. "+addCommas(voucher));
                  $(".courier_cost").text("Rp. "+addCommas(courier_cost));
                  $(".cost_handler").text("Rp. "+addCommas(cost_handler));
                  $(".grand_total").text("Rp. "+addCommas(total));

                  var format = "";
                  format += "Nama Pembeli: "+response.data['header'].gender_name+" "+response.data['header'].full_name;
                  format += "<br>No. Telp: "+response.data['header'].phone;
                  format += "<br>Alamat: "+response.data['header'].address_no;
                  format += "<br>Rt/Rw: "+response.data['header'].rt+"/"+response.data['header'].rw;
                  format += "<br>Desa: "+response.data['header'].village;
                  format += "<br>Patokan: "+response.data['header'].sub_district;
                  format += "<br>Kelurahan: "+response.data['header'].benchmark;
                  format += "<br>Kecamatan: "+response.data['header'].district;
                  format += "<br>Kota: "+response.data['header'].city_id;
                  format += "<br>Provinsi: "+response.data['header'].province_id;
                  format += "<br>Kode Pos: "+response.data['header'].postal_code;
                  format += "<br>";

                  $(".description").html(format);
                  $(".marketing_name").text("Marketing:"+response.data['header'].author);
                  $(".transaction_date").text(formatdate(response.data['header'].transaction_date));
                  $(".customer_address").text(response.data['header'].address);

                  $(".market_name").text(response.data['header'].market_name);
                  $(".courier_name").text(response.data['header'].courier_name);
                  $(".sales_order").html(response.data['header'].id+"<br><center>["+response.data['header'].confirm_status+"]</center>");
                }
                $(".datatable_detail").loading("stop");
            },
            error: function (xhr, status, error) {
              $(".datatable_detail").loading("stop");
            }
        });
      }
    });


  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {

                var format = "";
                format += "*LAPORAN PEMESANAN CS "+response.data.advertise_name.toUpperCase()+"*";
                format += "<br>*IKLAN : "+response.data.advertise_name.toUpperCase()+"*";
                format += "<br>*Tanggal : "+formatdate(response.data.transaction_date)+"*";
                // format += "<br>*Jam : 00.00 s/d 00.00*";
                format += "<br>";
                format += "<br>*Berikut data pengiriman pemesanan customer:*";
                format += "<br>*-Nama Pembeli:* "+response.data.gender_name+" "+response.data.full_name;
                format += "<br>*-No. Telp:* "+response.data.phone;
                format += "<br>*-Alamat:* "+response.data.address_no;
                format += "<br>*-Rt/Rw:* "+response.data.rt+"/"+response.data.rw;
                format += "<br>*-Desa:* "+response.data.village;
                format += "<br>*-Patokan:* "+response.data.sub_district;
                format += "<br>*-Kelurahan:* "+response.data.benchmark;
                format += "<br>*-Kecamatan:* "+response.data.district;
                format += "<br>*-Kota:* "+response.data.city_id;
                format += "<br>*-Provinsi:* "+response.data.province_id;
                format += "<br>*-Kode Pos:* "+response.data.postal_code;
                format += "<br>";

                var voucher = parseFloat(response.data.voucher_total);
                var quantity = parseFloat(response.data.quantity);
                var cost_handler = parseFloat(response.data.cost_handler);
                var price = parseFloat(response.data.price);
                var courier_cost = parseFloat(response.data.courier_cost);
                var insurance = parseFloat(response.data.insurance);
                var total = ((quantity*price)-voucher);
                var grandtotal = total+courier_cost+insurance+cost_handler;
                var vouchertemp = "Rp. "+parseFloat(voucher).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var quantitytemp = "Rp. "+parseFloat(quantity).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var cost_handlertemp = "Rp. "+parseFloat(cost_handler).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var pricetemp = "Rp. "+parseFloat(price).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var courier_costtemp = "Rp. "+parseFloat(courier_cost).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var insurancetemp = "Rp. "+parseFloat(insurance).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var totaltemp = "Rp. "+parseFloat(total).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                var grandtotaltemp = "Rp. "+parseFloat(grandtotal).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                format += "<br>*Berikut data Pesanan:*";
                format += "<br>*-No. Pesanan :* "+response.data.id.toUpperCase();
                format += "<br>*-Jumlah Pemesanan :* "+quantity;
                format += "<br>*-Biaya Voucher :* "+vouchertemp;
                format += "<br>*-Biaya Pesanan :* "+totaltemp;
                format += "<br>*-Biaya Pengiriman :* "+courier_costtemp;
                format += "<br>*-Biaya Asuransi :* Rp "+insurancetemp;
                format += "<br>*-Biaya Penanganan :* Rp "+cost_handlertemp;
                format += "<br>*-Total Pembayaran :* Rp "+grandtotaltemp;
                format += "<br>*-Market :* "+response.data.market_name;
                format += "<br>*-Bank   :* "+response.data.bank_name;
                format += "<br>*-Jenis transaksi :* "+response.data.payment_type_name;
                format += "<br>*-Kurir Pengiriman :* "+response.data.courier_name;
                format += "<br>*-Tanggal Order :* "+formatdate(response.data.transaction_date);
                format += "<br>*-No. Resi:* "+response.data.delivery_no;
                format += "<br>*-No. Ref:* "+response.data.delivery_refno;
                format += "<br>*-Status Pemesanan:* "+response.data.confirm_status;
                format += "<br>";
                format += "<br>*ACC*";
                format += "<br>*CS "+response.data.company_name+"* ✅";
                format += "<br>*Nama : "+response.data.created_name+"*";
                format += "<br>";
                format += "<br>*Packing*";
                format += "<br>*Nama :{{sess_user('name')}}*";
                copyToClipboard(format);
                var options = {
                  title:'Delete SO Confirmation?',
                  form_url:'{{ $path }}/delete/'+response.data.id,
                  form_method:'DELETE',
                  form_csrf:'{!! csrf_field() !!}'
                };
                showDialog.show(format,options);
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  }

  function export_do(isFormat){
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }
    $.ajax({
        url : '{{ $path }}/list',
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        data:{
            from_date : from_date,
            to_date : to_date,
            advertise_id : $("#advertise_id").val(),
            transaction_status : $("#transaction_status").val(),
            courier_id:$("#courier_id").val(),
            payment_type_id:$("#payment_type_id").val(),
            resi_status:$("#resi_status").val(),
            company_id:$("#company").val(),
            resi_status:$("#resi_status").val()
        },
        success: function (response) {
          var format = '';
          // format += "*Format Import Masal:*";
          // format += "<br>";
          // format += "SalesOrder";
          // format += "=Resi";
          // format += "=Refno";
          // format += "=Url";
          // format += "<br>";
          // format += "==========================";
          // format += "<br>";
          // for (var i in response.data){
          //   var rs = response.data[i];
          //   format += rs.id;
          //   format += "="+rs.delivery_no;
          //   format += "="+rs.delivery_refno;
          //   format += "="+rs.delivery_remark;
          //   format += "<br>";
          // }
          if(isFormat){
            format += "<br>";
            format += "*List No Order By Resi:*";
            format += "<br>";
            format += "==========================";
            format += "<br>";
            for (var i in response.data){
              var rs = response.data[i];
              format += rs.delivery_no;
              format += "<br>";
            }
          }else{
            format += "<br>";
            format += "*List No Order By E-Commerce:*";
            format += "<br>";
            format += "==========================";
            format += "<br>";
            for (var i in response.data){
              var rs = response.data[i];
              format += rs.id;
              format += "<br>";
            }
          }
          // format += "<br>   *(Replace this note!!)*";
          // format += "<br>";
          // format += "<br>*Terimakasih*";
          // format += "<br>*CS:{{sess_user('name')}}*";
          copyToClipboard(format);
          var img_url = '{{ENV('APP_URL')}}';
          var options = {
            title:'Laporan Kerja Marketing Harian',
            // imgUrl:img_url,
          };
          showDialog.show(format,options);
        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }


  function export_copy(){
    var url = "";
    var from_date = formatdate($("#datesearch_start").val());
    if(from_date){
      url += "&from_date="+from_date;
    }
    var to_date = formatdate($(" #datesearch_end").val(),true);
    if(to_date){
      url += "&to_date="+to_date;
    }
    var advertise_id = $("#advertise_id").val();
    if(advertise_id){
      url += "&advertise_id="+advertise_id;
    }
    var transaction_status = $("#transaction_status").val();
    if(transaction_status){
      url += "&transaction_status="+transaction_status;
    }
    var courier_id = $("#courier_id").val();
    if(courier_id){
      url += "&courier_id="+courier_id;
    }
    var company_id = $("#company").val();
    if(company_id){
      url += "&company_id="+company_id;
    }
    var resi_status = $("#resi_status").val();
    if(resi_status){
      url += "&resi_status="+resi_status;
    }

    $.ajax({
        url: '{{ $path }}/detail?do=all'+url,
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          var no = 0;
          var format = '';
          format += "*Laporan Gudang Harian: " +format+"*";
          format += "<br> *Nama Admin: {{sess_user('name')}}*";
          format += "<br> *Department: "+$("#company").find("option:selected").text()+"*";
          format += "<br> *========================*";

          var title = [];
          response.data.forEach(function (a) {
              if(title.indexOf(a.courier_name) === -1){
                  title.push(a.courier_name);
              }
          });
          var author = [];
          response.data.forEach(function (a) {
              if(author.indexOf(a.author) === -1){
                  author.push(a.author);
              }
          });

          for (var i in title){
            var titles = title[i]
            if(!title[i]){
              titles = "NOT SET";
            }
            var total_botol = 0;
            var total_packet = 0;
            format += "<br>";
            format += "*"+(Number(i)+1)+".) Kurir: "+titles+"*";
            var closing = "";
            for (var a in author){
              closing += "<br>  *"+(Number(a)+1)+".) Marketing: "+author[a]+"*";
              closing += "<br>";
              var no_closing = 0;
              for (var x in response.data){
                if(response.data[x].courier_name == title[i] && response.data[x].author == author[a]){
                  var voucher = parseFloat(response.data[x].voucher_total);
                  var quantity = parseFloat(response.data[x].quantity);
                  var cost_handler = parseFloat(response.data[x].cost_handler);
                  var price = parseFloat(response.data[x].price);
                  var courier_cost = parseFloat(response.data[x].courier_cost);
                  var insurance = parseFloat(response.data[x].insurance);
                  total_botol = (total_botol+quantity);
                  total_packet = (total_packet+1);

                  var total_product = ((quantity*price)-voucher);
                  var total = ((quantity*price)-voucher)+courier_cost+insurance+cost_handler;
                  var totaltemp = "Rp. "+parseFloat(total).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  var voucher_total = "Rp. "+parseFloat(voucher).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  var courier_total = "Rp. "+parseFloat(courier_cost+insurance).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                  no_closing = no_closing+1;
                  closing += "   "+no_closing+".) Resi   : "+response.data[x].delivery_no+"<br>";
                  closing += "         Cust      : "+response.data[x].full_name+"<br>";
                  closing += "         Telp      : "+response.data[x].phone+" Botol<br>";
                  closing += "         Jumlah    : "+parseInt(response.data[x].quantity)+"<br>";
                  closing += "         Voucher   : "+voucher_total+"<br>";
                  closing += "         Total   : Rp. "+addCommas(total_product)+"<br>";
                  closing += "         ongkir   : "+courier_total+"<br>";
                  closing += "         Grand Total  : "+totaltemp+"<br>";
                  closing += "         Status    : "+response.data[x].confirm_status+"<br>";
                }
              }
            }
            format += "<br> *#Total Paket: "+addCommas(total_packet);
            format += "<br> *#Total Botol: "+addCommas(total_botol);
            format += closing;
            closing = "";
          }
          format += "<br>";
          format += "<br>   *(Replace this note!!)*";
          format += "<br>";
          format += "<br>*Terimakasih*";
          format += "<br>*CS:{{sess_user('name')}}*";
          copyToClipboard(format);
          var img_url = '{{ENV('APP_URL')}}';
          var options = {
            title:'Laporan Pengiriman Harian',
            // imgUrl:img_url,
          };
          showDialog.show(format,options);
        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  $("#company").change(function(){
    $('#advertise_id').empty();
    $('#advertise_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','ads') }}/list?company_id=" + $("#company").val(),
        type: "GET",
        success: function (response) {
          $('#advertise_id').empty();
          $('#advertise_id').append('<option value="">Chose Advertise</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#advertise_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    $('#payment_type_id').empty();
    $('#payment_type_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','payment-type') }}/list?company_id=" + $("#company").val(),
        type: "GET",
        success: function (response) {
          $('#payment_type_id').empty();
          $('#payment_type_id').append('<option value="">Chose Payment</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#payment_type_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    $('#courier_id').empty();
    $('#courier_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','courier') }}/list?company_id=" + $("#company").val(),
        type: "GET",
        success: function (response) {
          $('#courier_id').empty();
          $('#courier_id').append('<option value="">Chose Courier</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#courier_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    refresh_table();
  });

  $("#payment_type_id, #transaction_status, #advertise_id, #resi_status, #courier_id").change(function(){
    refresh_table();
  });
  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }
</script>
@endsection
