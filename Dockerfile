FROM bourouq/alpine15-php74-nginx

WORKDIR /var/www/app
COPY . /var/www/app

RUN chmod -R 777 storage bootstrap

EXPOSE 80
