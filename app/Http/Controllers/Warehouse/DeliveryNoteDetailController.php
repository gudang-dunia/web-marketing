<?php

namespace App\Http\Controllers\Warehouse;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Resources\Sales\SalesOrder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class DeliveryNoteDetailController extends Controller {

    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $path_detail;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_detail;
    private static $resource_so;
    private static $resource_so_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'delivery-note';
        static::$module_alias = 'Delivery Note';
        static::$auth = 'delivery-note';
        static::$path = route('warehouse.index','delivery-note');
        static::$path_detail = route('warehouse.index','delivery-note-detail');
        static::$data = route('warehouse.list','delivery-note');
        static::$delete = route('warehouse.delete',['delivery-note','']);
        static::$controller = getControllerName("Warehouse", "delivery-note");
        static::$resource = getResourceName("Warehouse", "delivery-note");
        static::$resource_detail = getResourceName("Warehouse", "delivery-note-detail");
        static::$resource_so = getResourceName("Sales", "sales-order");
        static::$resource_so_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['path_detail'] = static::$path_detail;
      $data['data'] = static::$data;
      return view('warehouse.delivery-note',$data);
    }

    public static function data($id) {
        self::init();
        $module = static::$resource::withoutGlobalScopes(['active'])->where('company_id',sess_user('company_id'))->findOrFail($id);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $request->dln));
        $module = new static::$resource_detail();
        $module->id = $data->company_id.'-'.$data->id.'-'.$request->so_order;
        $module->company_id = $data->company_id;
        $module->delivery_note_id = $data->id;
        $module->sales_order_id = $request->so_order;
        $module->courier_id = $data->courier_id;
        $module->author = sess_user('name');
        $module->created_by = sess_user('id');
        $module->created_at = date('Y-m-d H:i');
        $module->save();


        $module = SalesOrder::find(str_replace('%20', ' ', $module->sales_order_id));
        if($module){
          $module->confirm_status = list_confirm_status()[3][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->save();
        }
        
        $module = Visitor::find(str_replace('%20', ' ', $module->id));
        if($module){
          $module->confirm_status = list_confirm_status()[3][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->save();
        }
        return makeResponse(200, 'success', null, $data);
    }

    public static function update($id, $request) {
        self::init();
        $data = static::$resource_detail::find(str_replace('%20', ' ', $id));
        $module = $data->delete();
        return makeResponse(200, 'success', null, $data);
    }

    public static function delete($id) {
        self::init();
        $data = static::$resource_detail::find(str_replace('%20', ' ', $id));
        $module = $data->delete();

        $module = Visitor::find(str_replace('%20', ' ', $data->sales_order_id));
        $module->confirm_status = list_confirm_status()[2][0];
        $module->confirm_by = sess_user('id');
        $module->confirm_date = date('Y-m-d H:i');
        $module->save();

        $module = SalesOrder::find(str_replace('%20', ' ', $data->sales_order_id));
        $module->confirm_status = list_confirm_status()[2][0];
        $module->confirm_by = sess_user('id');
        $module->confirm_date = date('Y-m-d H:i');
        $module->save();

        return makeResponse(200, 'success', null, $data);
    }

    public static function list($request) {
        self::init();
        $table = new static::$resource_detail();
        $result = \DB::table($table->getTable())
                  ->select(
                    $table->getTable().'.delivery_note_id as delivery_header'
                    ,$table->getTable().'.id as delivery_detail'
                    ,$table->getTable().'.status as delivery_status'
                    ,'sls_sales_order.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->join('sls_sales_order','sls_sales_order.id','=', $table->getTable().'.sales_order_id')
                  ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                  ->leftjoin('mst_customer','mst_customer.id','=','sls_sales_order.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=','sls_sales_order.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=','sls_sales_order.customer_address_id')
                  ->leftjoin('mst_bank','mst_bank.id','=','sls_sales_order.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=','sls_sales_order.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=','sls_sales_order.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=','sls_sales_order.payment_type_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=','sls_sales_order.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where($table->getTable().'.delivery_note_id',$request->dln)
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                        $query->where('sls_sales_order.company_id',$request->company_id);
                    }else{
                      $query->where('sls_sales_order.company_id',sess_user('company_id'));
                    }
                  })
                  ->where('sls_sales_order.id','!=',null)
                  ->groupBy('sls_sales_order.id')
                  ->get();
        return makeResponse(200, 'success', null, $result);
    }

    public static function detail($request) {
        self::init();
        $table = new static::$resource_so();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', $table->getTable().'.id')
                  ->leftjoin('ivt_delivery_note_detail','ivt_delivery_note_detail.sales_order_id','=', $table->getTable().'.id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where('ivt_delivery_note_detail.id','=',null)
                  ->where($table->getTable().'.confirm_status',list_confirm_status()[2][0])
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                        $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                  })
                  ->where(function ($query)use($request,$table) {
                    if ($request->courier_id) {
                      $query->where($table->getTable().'.courier_id',$request->courier_id);
                    }
                  })
                  ->where(function ($query)use($request,$table) {
                    if($request->list_so){
                      foreach ($request->list_so as $rs) {
                        if($rs){
                          $query->orWhere($table->getTable().'.'.$request->col_searching,$rs);
                        }
                      }
                    }
                  })
                  ->groupBy($table->getTable().'.id')
                  ->get();
        return makeResponse(200, 'success', null, $result);
    }
}
