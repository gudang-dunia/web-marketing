<?php

namespace App\Http\Controllers\Warehouse;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Resources\Product\Followup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PickingListController extends Controller {
    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'picking-list';
        static::$module_alias = 'Picking List';
        static::$auth = 'picking-list';
        static::$path = route('warehouse.index','picking-list');
        static::$data = route('warehouse.list','picking-list');
        static::$delete = route('warehouse.delete',['picking-list','']);
        static::$controller = getControllerName("Warehouse", "picking-list");
        static::$resource = getResourceName("Sales", "sales-order");
        static::$resource_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['data'] = static::$data;
      return view('warehouse.picking-list',$data);
    }

    public static function data($id) {
        static::init();
        $table = new static::$resource();
        $module = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', $table->getTable().'.id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where($table->getTable().'.id',$id)
                  ->first();
        return makeResponse(200, 'success', null, $module);
    }

    public static function update($id) {
        static::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('warehouse.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = Followup::find(str_replace('%20', ' ', $id));
        if($module){
          $module->confirm_status = list_confirm_status()[2][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->packing_id = sess_user('id');
          $module->packing_date = date('Y-m-d H:i');
          $module->save();
        }
        $module = Visitor::find(str_replace('%20', ' ', $id));
        if($module){
          $module->confirm_status = list_confirm_status()[2][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->packing_id = sess_user('id');
          $module->packing_date = date('Y-m-d H:i');
          $module->save();
        }
        $module = static::$resource::find(str_replace('%20', ' ', $id));
        if($module){
          $module->confirm_status = list_confirm_status()[2][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->packing_id = sess_user('id');
          $module->packing_date = date('Y-m-d H:i');
          $module->save();
        }
        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->id.'-'.$data->full_name.' has been lock!');
    }

    public static function list($request) {
        static::init();
        static::init();
        $table = new static::$resource();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->orWhere(function ($query)use($request,$table) {
                      $query->orWhere($table->getTable().'.packing_id',Null);
                      $query->orWhere($table->getTable().'.packing_id',sess_user('id'));
                  })
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                      $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                    if ($request->courier_id) {
                      $query->where($table->getTable().'.courier_id',$request->courier_id);
                    }
                    if ($request->sales_id) {
                      $query->where($table->getTable().'.sales_id',$request->sales_id);
                    }
                  })
                  ->where($table->getTable().'.confirm_status',list_confirm_status()[1][0]);

        if($request->from_date != '' && $request->to_date != ''){
          $result->where($table->getTable().'.transaction_date' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.transaction_date' ,'<=' , $request->to_date);
        }

        if($request->is_resi){
          $result->where($table->getTable().'.delivery_no' ,$request->is_resi,null);
        }

        $result->groupBy($table->getTable().'.id');
        $result->orderBy($table->getTable().'.status','ASC');
        $result->orderBy($table->getTable().'.transaction_date','DESC');

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('full_name', function($module) {
              return $module->gender_name." ".$module->full_name;
          })
          ->addColumn('active', function($module) {
              $created =  "transaction: ".date('d-m-Y H:i',strtotime($module->transaction_date))."<br/>";
              $status =  '<span class="label font-weight-bold label-lg  label-light-warning label-inline">'.$module->confirm_status.'</span>';
              $newold = $module->status ? '<span class="label font-weight-bold label-lg  label-light-danger label-inline">Lama</span>' : '<span class="label font-weight-bold label-lg  label-light-info label-inline">Baru</span>';
              return '<center>'.$created.$newold."&nbsp".$status.'</center>';
          })
          ->addColumn('action', function($module) {
              $id = "'".$module->id."'";
              $delete = '<div class="align-items-center bg-dark">
                            <a onclick="show_data('.$id.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="Picking SO" >process</a>
                        </div>';
              if($module->closing_status){
                return '<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> Data Closed</span>';
              }else{
                  return $delete;
              }
          })
          ->rawColumns(['active', 'action'])
          ->make(true);
    }

    public static function detail($request) {
        static::init();
        $table = new static::$resource();
        $result['header'] = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,'mst_bank.id as bank_id'
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,'mst_market.id as market_id'
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,'mst_courier.id as courier_id'
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,'mst_payment_type.id as payment_type_id'
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,'mst_voucher.id as voucher_id'
                    ,'mst_voucher.name as voucher_name'
                    ,'mst_voucher.total as voucher_price'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('mst_voucher','mst_voucher.id','=', $table->getTable().'.voucher_id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->join('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                        $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                  })
                  ->where($table->getTable().'.id',$request->sales_order_id)
                  ->first();

        $table = new static::$resource_detail();
        $result['detail'] = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                  ,'mst_company.name as company_name'
                  ,'mst_item.name as item_name'
                  )
                  ->leftjoin('mst_item','mst_item.id','=', $table->getTable().'.item_id')
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->where($table->getTable().'.sales_order_id',$request->sales_order_id)
                  ->orderBy($table->getTable().'.id','ASC')
                  ->get();
        return makeResponse(200, 'success', null, $result);
    }

}
