<?php

namespace App\Http\Controllers\Warehouse;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class DashboardController extends Controller {
    private static $module;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;
    public static function init(){
        static::$module = 'Dashboard Admin Product';
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }
    public static function index($request){
        static::init();
        $start_date = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $end_date = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $dash = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->orderBy('sls_product_visitor.created_at','DESC')
                    ->get();
        $dash_track = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.confirm_status','!=',null)
                    ->where('sls_product_visitor.transaction_date','>=' , $start_date)
                    ->where('sls_product_visitor.transaction_date','<=' , $end_date)
                    ->orderBy('sls_product_visitor.transaction_date','DESC')
                    ->get();
        $dash1 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',0)
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash2 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',1)
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash3 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',0)
                    ->where('sls_product_visitor.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash4 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',1)
                    ->where('sls_product_visitor.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();

        $dash5 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('count(sls_sales_order.id) as jml'),
                    \DB::raw('sls_sales_order.confirm_status as status')
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.confirm_status','!=',null)
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->groupBy('sls_sales_order.confirm_status')
                    ->get();
        $dash6 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('count(sls_sales_order.confirm_status) as jml'),
                    \DB::raw('sls_sales_order.confirm_status as status'),
                    \DB::raw('IFNULL(users.name,sls_sales_order.author) as author')
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->join('users','users.id','=', 'sls_sales_order.sales_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->groupBy('sls_sales_order.sales_id')
                    ->groupBy('sls_sales_order.confirm_status')
                    ->orderBy('sls_sales_order.sales_id','ASC')
                    ->get();
        $dash7 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('IFNULL(users.name,sls_sales_order.author) as author')
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->join('users','users.id','=', 'sls_sales_order.sales_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->groupBy('sls_sales_order.sales_id')
                    ->orderBy('sls_sales_order.sales_id','ASC')
                    ->get();
        $dash8 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('IFNULL(COUNT(sls_sales_order.id),0) as jml')
                    ,\DB::raw('IFNULL(COUNT(sls_sales_order.id),0)*1000 as packing_commission')
                    )
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->first();

        $dash9 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('IFNULL(SUM(sls_sales_order.transaction),0) as grand_total')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total')
                    ,\DB::raw('IFNULL(IFNULL(mst_commission_rate.commission_price,mst_company.commission_price),sls_sales_order_detail.commission_price) as commission_price')
                    )
                    ->leftjoin('mst_commission_rate', function ($join) {
                        $join->on('mst_commission_rate.sales_id', '=', 'sls_sales_order.sales_id');
                        $join->on('sls_sales_order.transaction_date', '>=', 'mst_commission_rate.start_date');
                        $join->on('sls_sales_order.transaction_date', '<=', 'mst_commission_rate.end_date');
                    })
                    ->join('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->first();

        $dash10 = \DB::table('sls_sales_order')
                  ->select(
                  \DB::raw('IFNULL(SUM(sls_sales_order.transaction),0) as grand_total')
                  ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                  ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                  ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                  ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total')
                  ,\DB::raw('IFNULL(IFNULL(mst_commission_rate.commission_price,mst_company.commission_price),sls_sales_order_detail.commission_price) as commission_price')
                  )
                  ->leftjoin('mst_commission_rate', function ($join) {
                      $join->on('mst_commission_rate.sales_id', '=', 'sls_sales_order.sales_id');
                      $join->on('sls_sales_order.transaction_date', '>=', 'mst_commission_rate.start_date');
                      $join->on('sls_sales_order.transaction_date', '<=', 'mst_commission_rate.end_date');
                  })
                  ->join('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                  ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                  ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                  ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                  ->where(function ($query)use($request) {
                    if($request->company_id){
                        $query->where('sls_sales_order.company_id',$request->company_id);
                    }else{
                      $query->where('sls_sales_order.company_id',sess_user('company_id'));
                    }
                  })
                  ->where(function ($where) {
                    $where->orwhere('sls_sales_order.confirm_status',list_confirm_status()[0][0]);
                    $where->orwhere('sls_sales_order.confirm_status',list_confirm_status()[1][0]);
                    $where->orwhere('sls_sales_order.confirm_status',list_confirm_status()[2][0]);
                    $where->orwhere('sls_sales_order.confirm_status',list_confirm_status()[3][0]);
                  })
                  ->where('sls_sales_order.created_at','>=' , $start_date)
                  ->where('sls_sales_order.created_at','<=' , $end_date)
                  ->first();

      $dash11 = \DB::table('sls_sales_order')
                  ->select(
                  \DB::raw('count(sls_sales_order.id) as jml'),
                  \DB::raw('sls_sales_order.confirm_status as status')
                  )
                  ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                  ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                  ->where(function ($query)use($request) {
                    if($request->company_id){
                        $query->where('sls_sales_order.company_id',$request->company_id);
                    }else{
                      $query->where('sls_sales_order.company_id',sess_user('company_id'));
                    }
                  })
                  ->where('sls_sales_order.confirm_status','!=',null)
                  ->where('sls_sales_order.created_at','>=' , $start_date)
                  ->where('sls_sales_order.created_at','<=' , $end_date)
                  ->groupBy('sls_sales_order.confirm_status')
                  ->get();

        $data["module"] = static::$module;
        $data["dash"] = $dash ? $dash:null;
        $data["dash_track"] = $dash_track ? $dash_track:null;
        $data["dash1"] = $dash1 ? ["jml"=>$dash1->jml,"name"=>"Interaksi Lead"]:["jml"=>0,"name"=>"Interaksi Lead"];
        $data["dash2"] = $dash2 ? ["jml"=>$dash2->jml,"name"=>"Interaksi Followup"]:["jml"=>0,"name"=>"Interaksi Followup"];
        $data["dash3"] = $dash3 ? ["jml"=>$dash3->jml,"name"=>"Closing Lead"]:["jml"=>0,"name"=>"Closing Lead"];
        $data["dash4"] = $dash4 ? ["jml"=>$dash4->jml,"name"=>"Closing Followup"]:["jml"=>0,"name"=>"Closing Followup"];
        $data["dash5"] = $dash5;
        $data["dash6"] = $dash6;
        $data["dash7"] = $dash7;
        $data["dash8"] = $dash8;
        $data["dash9"] = $dash9;
        $data["dash10"] = $dash10;
        $data["dash11"] = $dash11;
        $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
        $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
        return view('warehouse.dashboard',$data);
    }

}
