<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function noauth(){
        return view('auth.noauth');
    }

    public function index()
    {
        return redirect(sess_user('dashboard_url'));
    }

    public function dashboard()
    {
      if(sess_user('dashboard_url') != 'dashboard'){
        return redirect(sess_user('dashboard_url'));
      }
      return view('dashboard');
    }
}
