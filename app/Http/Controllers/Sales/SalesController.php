<?php
namespace App\Http\Controllers\Sales;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function index($table, Request $request)
    {
      return getControllerName("Sales", $table)::index($request);
    }

    public function list($table, Request $request)
    {
      return getControllerName("Sales", $table)::list($request);
    }

    public function detail($table, Request $request)
    {
      return getControllerName("Sales", $table)::detail($request);
    }

    public function data($table, $id)
    {
      return getControllerName("Sales", $table)::data($id);
    }

    public function save($table, Request $request)
    {
      return getControllerName("Sales", $table)::save($request);
    }

    public function update($table, $id, Request $request)
    {
      return getControllerName("Sales", $table)::update($id, $request);
    }

    public function delete($table,$id)
    {
      return getControllerName("Sales", $table)::delete($id);
    }

    public function cancel($table,$id)
    {
      return getControllerName("Sales", $table)::cancel($id);
    }

}
