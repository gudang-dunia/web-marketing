<?php

namespace App\Http\Controllers\Sales;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SalesRequestController extends Controller {
    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_header;
    private static $resource_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'sales-request';
        static::$module_alias = 'Sales Request';
        static::$auth = 'sales-request';
        static::$path = route('sales.index','sales-request');
        static::$data = route('sales.list','sales-request');
        static::$delete = route('sales.delete',['sales-request','']);
        static::$controller = getControllerName("Sales", "sales-request");
        static::$resource = getResourceName("Product", "visitor");
        static::$resource_header = getResourceName("Sales", "sales-order");
        static::$resource_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['data'] = static::$data;
      return view('sales.sales-request',$data);
    }

    public static function data($id) {
        static::init();
        $table = new static::$resource();
        $module = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,\DB::raw('IFNULL(mst_company.name,"") as company_name')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where($table->getTable().'.id',$id)
                  ->first();
        return makeResponse(200, 'success', null, $module);
    }

    public static function update($id) {
        static::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = Visitor::find(str_replace('%20', ' ', $id));
        if($module){
          $module->lock_status = 1;
          $module->status = 1;
          $module->confirm_status = list_confirm_status()[0][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->save();
          $module->status = $module->followup_status;

          $header = static::$resource_header::find(str_replace('%20', ' ', $id));
          $header = static::execute_header($module,$header);
          $detail = new static::$resource_detail();
          $detail->sales_order_id = $module->id;
          $detail->sales_id = $module->sales_id;
          $detail->author = $module->author;
          $detail->created_by = $module->created_by;
          $detail->voucher_id = $module->voucher_id;
          $detail->item_id = $module->item_id;
          $detail->voucher_id = $module->voucher_id;
          $detail->quantity = $module->quantity;
          $detail->price = $module->price;
          $detail->voucer = $module->voucher_total;
          $detail->status = $module->status;
          $transaction = ($detail->quantity*$module->price);
          $detail->total_transaction = ($transaction-$module->voucher_total);
          $so_detail = static::$resource_detail::where('sales_order_id',$module->id)->delete();
          $detail = static::execute_detail($detail);
        }

        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->id.'-'.$data->full_name.' has been lock!');
    }

    public static function list($request) {
        static::init();
        $table = new static::$resource();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_gender.name as gender_name'
                    ,'mst_advertise.name as advertise_name'
                    ,\DB::raw('IFNULL(mst_company.name,"") as company_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as customer_address')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->join('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->join('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->join('mst_company','mst_company.id','=', 'mst_customer.company_id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where($table->getTable().'.lock_status',0)
                  ->where($table->getTable().'.status',1)
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                      $query->where($table->getTable().'.company_id',$request->company_id);
                    }
                    if ($request->sales_id) {
                      $query->where($table->getTable().'.sales_id',$request->sales_id);
                    }
                    if ($request->courier_id) {
                      $query->where($table->getTable().'.courier_id',$request->courier_id);
                    }
                  });

        if($request->from_date != '' && $request->to_date != ''){
          $result->where($table->getTable().'.transaction_date' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.transaction_date' ,'<=' , $request->to_date);
        }
        if($request->check_so_phone AND $request->check_so_customer){
          $result->where(function ($query)use($request,$table){
            $query->orwhere('mst_customer.phone',$request->check_so_phone);
            $query->orwhere('mst_customer.full_name',$request->check_so_customer);
            $query->where($table->getTable().'.confirm_status','!=',list_confirm_status()[4][0]);
          });
        }

        $result->orderBy($table->getTable().'.confirm_status','ASC');
        $result->orderBy($table->getTable().'.transaction_date','DESC');

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('full_name', function($module) {
              return $module->gender_name." ".$module->full_name;
          })
          ->addColumn('status', function($module) {
              $created =  "transaction: ".date('d-m-Y H:i',strtotime($module->transaction_date))."<br/>";
              $newold = $module->followup_status ? '<span class="label font-weight-bold label-lg  label-light-danger label-inline">Lama</span>' : '<span class="label font-weight-bold label-lg  label-light-info label-inline">Baru</span>';
              return '<center>'.$created.$newold.'</center>';
          })
          ->addColumn('action', function($module) {
              $id = "'".$module->id."'";
              $full_name = "'".$module->full_name."'";
              $phone = "'".$module->phone."'";
              $process = '<div class="align-items-center bg-dark">
                            <a onclick="check_data('.$id.','.$full_name.','.$phone.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="Check Valid SO" >Check SO</a>
                            &nbsp;
                            <a onclick="show_data('.$id.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="Approval SO" >Process</a>
                        </div>';
              if($module->closing_status){
                return '<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> Data Closed</span>';
              }else{
                  return $process;
              }
          })
          ->rawColumns(['status', 'action'])
          ->make(true);
    }

    public static function execute_header($request, $data = null) {
       static::init();
       if (is_null($data)) {
           $data = new static::$resource_header();
           $data->created_at = currDate();
       }else{
           $data->updated_at = currDate();
       }

       if ($request->company_id){
         $data->company_id = $request->company_id;
       }else{
         $data->company_id = sess_user('company_id');
       }

       if ($request->customer_id){
         $data->customer_id = $request->customer_id;
       }

       if ($request->customer_address_id){
         $data->customer_address_id = $request->customer_address_id;
       }

       if ($request->id) {
           $data->id = strtoupper($request->id);
       }

       if ($request->advertiser_name){
         $data->advertiser_name = $request->advertiser_name;
       }

       if ($request->shift_work_id){
         $data->shift_work_id = $request->shift_work_id;
       }

       if ($request->advertise_id){
         $data->advertise_id = $request->advertise_id;
       }
       if ($request->interaction_id){
         $data->interaction_id = $request->interaction_id;
       }
       if ($request->courier_cost){
         $data->courier_cost = $request->courier_cost;
       }
       if ($request->insurance){
         $data->insurance = $request->insurance;
       }
       if ($request->cost_handler){
         $data->cost_handler = $request->cost_handler;
       }
       if ($request->voucher_total){
         $data->voucher_total = $request->voucher_total;
       }
       if ($request->transaction){
         $data->transaction = $request->transaction;
       }
       if ($request->voucher_id){
         $data->voucher_id = $request->voucher_id;
       }
       if ($request->market_id){
         $data->market_id = $request->market_id;
       }
       if ($request->courier_id){
         $data->courier_id = $request->courier_id;
       }
       if ($request->payment_type_id){
         $data->payment_type_id = $request->payment_type_id;
       }
       if ($request->bank_id){
         $data->bank_id = $request->bank_id;
       }
       if ($request->transaction_date){
           $data->transaction_date = $request->transaction_date;
       }
       if ($request->packing_id){
         $data->packing_id = $request->packing_id;
       }
       if ($request->packing_date){
             $data->packing_date = $request->packing_date;
       }
       if ($request->delivery_no){
         $data->delivery_no = $request->delivery_no;
       }
       if ($request->delivery_refno){
         $data->delivery_refno = $request->delivery_refno;
       }
       if ($request->delivery_remark){
         $data->delivery_remark = $request->delivery_remark;
       }
       if ($request->delivery_date){
           $data->delivery_date = $request->delivery_date;
       }
       if ($request->confirm_status){
         $data->confirm_status = $request->confirm_status;
       }
       if ($request->confirm_by){
         $data->confirm_by = $request->confirm_by;
       }
       if ($request->confirm_date){
           $data->confirm_date = $request->confirm_date;
       }
       if ($request->closing_status){
         $data->closing_status = $request->closing_status;
       }
       if ($request->closing_by){
         $data->closing_by = $request->closing_by;
       }
       if ($request->closing_date){
           $data->closing_date = $request->closing_date;
       }
       if ($request->img_transaction){
         $data->img_transaction = $request->img_transaction;
       }
       if ($request->img_packing){
         $data->img_packing = $request->img_packing;
       }
       if ($request->img_delivery){
         $data->img_delivery = $request->img_delivery;
       }
       if ($request->author){
         $data->author = $request->author;
       }
       if ($request->sales_id){
         $data->sales_id = $request->sales_id;
       }
       if ($request->status) {
           $data->status = to_bool($request->status);
       }
       return $data->save();
   }

   public static function execute_detail($request, $data = null) {
      static::init();
      if (is_null($data)) {
          $data = new static::$resource_detail();
          $data->created_at = currDate();
      }else{
          $data->updated_at = currDate();
      }

      if ($request->company_id){
        $data->company_id = $request->company_id;
      }else{
        $data->company_id = sess_user('company_id');
      }
      if ($request->sales_order_id){
        $data->sales_order_id = $request->sales_order_id;
      }
      if ($request->id) {
          $data->id = strtoupper($request->id);
      }else{
          $data->id = strtoupper(generadeCode("Sales","sales-order-detail",$data->sales_order_id, null, $numb=3));
      }
      if ($request->voucher_id){
        $data->voucher_id = $request->voucher_id;
      }
      if ($request->item_id){
        $data->item_id = $request->item_id;
      }
      if ($request->quantity){
        $data->quantity = $request->quantity;
      }
      if ($request->price){
        $data->price = $request->price;
      }
      if ($request->voucer){
        $data->voucer = $request->voucer;
      }
      if ($request->total_transaction){
        $data->total_transaction = $request->total_transaction;
      }
      if ($request->author){
        $data->author = $request->author;
      }
      if ($request->sales_id){
        $data->sales_id = $request->sales_id;
      }
      if ($request->created_by){
        $data->created_by = $request->created_by;
      }
      if ($request->created_by){
        $data->created_by = $request->created_by;
      }
      if ($request->updated_by){
        $data->updated_by = $request->updated_by;
      }

      return $data->save();
  }

}
