<?php

namespace App\Http\Controllers\Reports;

use DataTables;
use Anam\PhantomMagick\Converter;
use PdfReport;
use ExcelReport;
use CSVReport;
use PDF;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Menu;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;


class ProductSalesClosingController extends Controller implements ToCollection{

    public function collection(Collection $rows){
        foreach ($rows as $row)
        {
            User::create([
                'name' => $row[0],
            ]);
        }
    }

    // public static function export ($table, $import, $is_reqs, $request) {
    //   (new ProductSalesClosingController)->import('users.xlsx', null, \Maatwebsite\Excel\Excel::XLSX);
    // }

    public static function export ($table, $export, $is_reqs, $request) {
      $query = \DB::table('sls_sales_order')
                  ->select('sls_sales_order.*'
                    ,'mst_company.name as company_name'
                    ,'mst_company.contact_person as company_contact_person'
                    ,'mst_company.phone as company_phone'
                    ,'mst_company.fax as company_fax'
                    ,'mst_company.city as company_city'
                    ,'mst_company.address as company_address'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_item.name as product_name'
                    ,'mst_customer.phone as phone'
                    ,\DB::raw('CONCAT(mst_gender.name," ",mst_customer.full_name) as full_name')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM((sls_sales_order_detail.quantity*sls_sales_order_detail.price)-sls_sales_order_detail.voucer),0) as total')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(DATE_FORMAT(sls_sales_order_closing.closing_date, "%d-%M-%Y"),"") as closing_date')
                    ,\DB::raw('IFNULL(DATE_FORMAT(ivt_delivery_note.delivery_date, "%d-%M-%Y"),"") as delivery_date')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                    ,\DB::raw('"EZ" as default_code')
                    ,\DB::raw('0 as default_int')
                    ,\DB::raw('"" as default_str')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', 'sls_sales_order.id')
                  ->leftjoin('sls_sales_order_closing_detail','sls_sales_order_closing_detail.sales_order_id','=', 'sls_sales_order.id')
                  ->leftjoin('sls_sales_order_closing','sls_sales_order_closing_detail.sales_order_closing_id','=', 'sls_sales_order_closing.id')
                  ->leftjoin('ivt_delivery_note_detail','ivt_delivery_note_detail.sales_order_id','=', 'sls_sales_order.id')
                  ->leftjoin('ivt_delivery_note','ivt_delivery_note_detail.delivery_note_id','=', 'ivt_delivery_note.id')
                  ->leftjoin('mst_item','mst_item.id','=', 'sls_sales_order_detail.item_id')
                  ->leftjoin('mst_bank','mst_bank.id','=', 'sls_sales_order.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', 'sls_sales_order.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', 'sls_sales_order.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', 'sls_sales_order.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', 'sls_sales_order.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', 'sls_sales_order.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where('sls_sales_order.transaction_date','>=' , $request->from_date)
                  ->where('sls_sales_order.transaction_date','<=' , $request->to_date)
                  ->where(function ($query)use($request) {
                    if($request->company_id){
                        $query->where('sls_sales_order.company_id',$request->company_id);
                    }
                  })
                  ->groupBy('sls_sales_order.id')
                  ->orderBy('sls_sales_order.company_id','ASC')
                  ->orderBy('sls_sales_order.transaction_date','DESC');

      if($request->courier_id){
        $query->where('sls_sales_order.courier_id' , $request->courier_id);
      }

      if($request->resi_status){
        if($request->resi_status == 'off'){
          $query->where('sls_sales_order.delivery_no', '=' , null);
        }else{
          $query->where('sls_sales_order.delivery_no', '!=' , null);
        }
      }
      $filename = "Export Excel";
      if($request->filename){
        $filename = $request->filename;
      }
      $title = 'DATA TRANSAKSI '.date('d-m-Y H:i', strtotime($request->from_date)).'-'.date('d-m-Y H:i', strtotime($request->to_date));
    	$meta = [
    		'Created' => sess_user('name'),
    		'Desc' => 'ASC'
    	];
      $columns = [
          'Company' =>  'company_name',
          'Market' =>  'market_name',
          'Iklan' =>  'advertise_name',
          'Advertiser' =>  'advertiser_name',
          'No Pesanan' =>  'id',
          'No Resi' =>  'delivery_refno',
          'Status Closing' =>  'status',
          'Nama CS' =>  'author',
          'Resi' =>  'delivery_no',
          'Bank' =>  'bank_name',
          'Kurir' =>  'courier_name',
          'Metode' =>  'payment_type_name',
          'Nama customer' =>  'full_name',
          'Alamat' =>  'address',
          'Kota' =>  'city_id',
          'Provinsi' =>  'province_id',
          'No telp' =>  'phone',
          'Jml' =>  'quantity',
          'Harga' =>  'price',
          'Ongkir' =>  'courier_cost',
          'Asuransi' =>  'insurance',
          'Total Harga' =>  'total_transaction',
          'Tanggal order' =>  'transaction_date',
          'Delivery Status' =>  'confirm_status',
          'Delivery Date' =>  'delivery_date',
          'Closing Date' =>  'closing_date',
      ];
      $data['data_list'] = $query;
      $data['meta'] = $meta;
      $data['columns'] = $columns;
      $data['title'] = $title;
      $data['file_name'] = $filename;
      $data['from_date'] = date('d/m/Y H:i', strtotime($request->from_date));
      $data['to_date'] = date('d/m/Y H:i', strtotime($request->to_date));
      $data['is_btn'] = $is_reqs;
      $data['btn_url'] = route('reports.download',['ProductSales',$export]);
      return static::generade($export, $data);
    }

    public static function generade($export, $data) {
      $exp = ExcelReport::of($data["title"], $data["meta"], $data["data_list"], $data["columns"]);
      $exp->showMeta(false);
      $exp->setCss(['.bolder' => 'font-weight: 800;','.italic-red' => 'color: red;font-style: italic;']);
      $exp->make();
      return $exp->download($data["file_name"].'xlsx');
    }
}
