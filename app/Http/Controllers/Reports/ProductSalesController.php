<?php

namespace App\Http\Controllers\Reports;

use DataTables;
use Anam\PhantomMagick\Converter;
use PdfReport;
use ExcelReport;
use CSVReport;
use PDF;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Menu;

class ProductSalesController extends Controller {

    public static function export ($table, $export, $is_reqs, $request) {
      $query = \DB::table('sls_sales_order')
                  ->select('sls_sales_order.*'
                    ,'mst_company.name as company_name'
                    ,'mst_company.contact_person as company_contact_person'
                    ,'mst_company.phone as company_phone'
                    ,'mst_company.fax as company_fax'
                    ,'mst_company.city as company_city'
                    ,'mst_company.address as company_address'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_item.name as product_name'
                    ,'mst_customer.phone as phone'
                    ,\DB::raw('(CASE WHEN LOWER(mst_payment_type.name) = "tf" OR LOWER(mst_payment_type.name) = "transfer" THEN "" ELSE sls_sales_order.transaction END) AS transaction')
                    ,\DB::raw('RIGHT(REPLACE(sls_sales_order.id,"-","" ),20) as id_jne')
                    ,\DB::raw('CONCAT(mst_gender.name," ",mst_customer.full_name) as full_name')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM((sls_sales_order_detail.quantity*sls_sales_order_detail.price)-sls_sales_order_detail.voucer),0) as total')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                    ,\DB::raw('"EZ" as default_code')
                    ,\DB::raw('0 as default_int')
                    ,\DB::raw('"" as default_str')
                    ,\DB::raw('"Paket" as default_packet')
                    ,\DB::raw('"TANGRANG" as default_province')
                    ,\DB::raw('"N" as default_insurance')
                    ,\DB::raw('"BARANG" as default_item_type')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', 'sls_sales_order.id')
                  ->leftjoin('mst_item','mst_item.id','=', 'sls_sales_order_detail.item_id')
                  ->leftjoin('mst_bank','mst_bank.id','=', 'sls_sales_order.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', 'sls_sales_order.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', 'sls_sales_order.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', 'sls_sales_order.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', 'sls_sales_order.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', 'sls_sales_order.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where('sls_sales_order.transaction_date','>=' , $request->from_date)
                  ->where('sls_sales_order.transaction_date','<=' , $request->to_date)
                  ->where(function ($query)use($request) {
                    if($request->company_id){
                        $query->where('sls_sales_order.company_id',$request->company_id);
                    }else{
                      $query->where('sls_sales_order.company_id',sess_user('company_id'));
                    }
                  })
                  ->groupBy('sls_sales_order.id')
                  ->orderBy('sls_sales_order.transaction_date','DESC');

      if($request->courier_id){
        $query->where('sls_sales_order.courier_id' , $request->courier_id);
      }

      if($request->resi_status){
        if($request->resi_status == 'off'){
          $query->where('sls_sales_order.delivery_no', '=' , null);
        }else{
          $query->where('sls_sales_order.delivery_no', '!=' , null);
        }
      }
      $filename = "Export Excel";
      if($request->filename){
        $filename = $request->filename;
      }
      $title = 'DATA TRANSAKSI '.date('d-m-Y H:i', strtotime($request->from_date)).'-'.date('d-m-Y H:i', strtotime($request->to_date));
    	$meta = [
    		'Created' => sess_user('name'),
    		'Desc' => 'ASC'
    	];

      if ($request->courier=="JNE") {
        $columns = [
            'RECEIVER NAME' => 'full_name',
            'RECEIVER ADDRESS' => 'address',
            'RECEIVER CITY' => 'city_id',
            'RECEIVER ZIP' => 'postal_code',
            'RECEIVER REGION' => 'province_id',
            'RECEIVER CONTACT' => 'full_name',
            'RECEIVER PHONE' => 'phone',
            'QTY*' => 'quantity',
            'WEIGHT*' => 'default_int',
            'GOODS DESC*' => 'product_name',
            'GOODS VALUE' => 'default_packet',
            'SPECIAL INSTRUCTION' => 'default_str',
            'SERVICE*' => 'default_str',
            'ORDER ID/REFERENCE NUMBER*' => 'id_jne',
            'INSURANCE*' => 'default_insurance',
            'SHIPPER NAME*' => 'full_name',
            'SHIPPER ADDRESS*' => 'company_address',
            'SHIPPER CITY*' => 'company_city',
            'SHIPPER ZIP' => 'default_str',
            'SHIPPER REGION*' => 'default_province',
            'SHIPPER CONTACT' => 'company_contact_person',
            'SHIPPER PHONE*' => 'company_phone',
            'DESTINATION_CODE' => 'company_city',
          ];
      }else{
        $columns = [
            'Berat' =>  'default_int',
            'Nama Pengirim' =>  'company_contact_person',
            'Telepon Pengirim'  =>  'company_phone',
            'Kota Pengirim' =>  'company_city',
            'Alamat Pengirim' =>  'company_address',
            'Apakah Dropship?' =>  'default_int',
            'Nama Dropshiper' =>  'default_str',
            'Telfon Dropshiper' =>  'default_str',
            'Nama Penerima' =>  'full_name',
            'Telepon Penerima'  =>  'phone',
            'Kecamatan' =>  'district',
            'Alamat Penerima' =>  'address',
            'Nama Barang' =>  'product_name',
            'Nilai Barang'  =>  'total',
            'Apakah Input Asuransi?' =>  'default_int',
            'Jumlah'  =>  'quantity',
            'Jenis Barang'  =>  'default_item_type',
            'Keterangan'  =>  'default_str',
            'Nomor pesanan e-commerce'  =>  'id',
            'COD' =>  'transaction',
            'Jenis Layanan' =>  'default_code',
            'Biaya Pengiriman'  =>  'courier_cost',
            'Biaya Lainnya' =>  'default_int',
        ];
      }
      $data['data_list'] = $query;
      $data['meta'] = $meta;
      $data['columns'] = $columns;
      $data['title'] = $title;
      $data['file_name'] = $filename;
      $data['from_date'] = date('d/m/Y H:i', strtotime($request->from_date));
      $data['to_date'] = date('d/m/Y H:i', strtotime($request->to_date));
      $data['is_btn'] = $is_reqs;
      $data['btn_url'] = route('reports.download',['ProductSales',$export]);
      return static::generade($export, $data);
    }

    public static function generade($export, $data) {
      $exp = ExcelReport::of($data["title"], $data["meta"], $data["data_list"], $data["columns"]);
      $exp->showMeta(false);
      $exp->make();
      return $exp->download($data["file_name"].'xlsx');
    }
}
