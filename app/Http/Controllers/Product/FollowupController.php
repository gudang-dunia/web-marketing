<?php

namespace App\Http\Controllers\Product;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Resources\Product\Followup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class FollowupController extends Controller {
    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'Followup';
        static::$module_alias = 'Followup';
        static::$auth = 'Followup';
        static::$path = route('product.index','Followup');
        static::$data = route('product.list','Followup');
        static::$delete = route('product.delete',['Followup','']);
        static::$controller = getControllerName("Product", "Followup");
        static::$resource = getResourceName("Product", "Followup");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['path_customer'] = route('master.index','Customer');
      $data['path_customer_address'] = route('master.index','CustomerAddress');
      $data['data'] = static::$data;
      return view('product.followup',$data);
    }

    public static function data($id) {
        static::init();
        $module = Followup::select(static::$table->getTable().'.*'
                  ,'mst_customer.full_name as full_name'
                  ,'mst_customer.phone as phone'
                  ,'mst_gender.id as gender_id'
                  ,'mst_gender.name as gender_name'
                  ,'mst_advertise.name as advertise_name'
                  )
                  ->join('mst_customer','mst_customer.id','=', static::$table->getTable().'.customer_id')
                  ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                  ->findOrFail($id);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        static::init();
        $validator = static::$controller::validation($request);
        if ($validator->fails()) return redirect()->route('product.index',static::$auth)->with('notif_danger', 'New '.static::$module_alias.' '. $request->full_name .' can not be save!');

        $module = static::$controller::execute($request);

        if($module->customer_id){
          $m_customer = $request;
          $m_customer->id = $module->customer_id;
          getResourceName("Master", "Customer")::destroy($module->customer_id);
          getControllerName("Master", "Customer")::execute($m_customer);
        }

        if($module->customer_address_id){
          $m_customer_address = $request;
          $m_customer_address->id = $module->customer_address_id;
          getResourceName("Master", "CustomerAddress")::destroy($module->customer_address_id);
          getControllerName("Master", "CustomerAddress")::execute($m_customer_address);
        }

        Visitor::destroy($module->id);
        $visitor = new Visitor($module->getOriginal());
        $visitor->save();
        if($module->status){
          Followup::destroy($module->id);
        }
        return redirect()->route('product.index',static::$auth)->with('notif_success', 'New '.static::$module_alias.' '. $request->full_name .' has been added successfully!');
    }

    public static function update($id, $request) {
        static::init();
        $validator = static::$controller::validation($request,'update');
        if ($validator->fails()) return redirect()->route('product.index',static::$auth)->with('notif_danger', 'New '.static::$module_alias.' '. $request->full_name .' can not be udate!');

        $data = Followup::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('product.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = static::$controller::execute($request,$data);

        if($module->customer_id){
          $m_customer = $request;
          $m_customer->id = $module->customer_id;
          getResourceName("Master", "Customer")::destroy($module->customer_id);
          getControllerName("Master", "Customer")::execute($m_customer);
        }

        if($module->customer_address_id){
          $m_customer_address = $request;
          $m_customer_address->id = $module->customer_address_id;
          getResourceName("Master", "CustomerAddress")::destroy($module->customer_address_id);
          getControllerName("Master", "CustomerAddress")::execute($m_customer_address);
        }

        Visitor::destroy($module->id);
        $visitor = new Visitor($module->getOriginal());
        $visitor->save();

        if($module->status){
          Followup::destroy($module->id);
        }

        return redirect()->route('product.index',static::$auth)->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been update successfully!');
    }

    public static function delete($id) {
        static::init();
        $data = Followup::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('product.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = $data->delete();

        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been deleted!');
    }

    public static function validation($request, $type = null) {
         $rules = [
             'full_name' => 'required|max:250',
         ];
         return Validator::make($request->all(), $rules);
   }

    public static function list($request) {
        static::init();
        $table = new Followup();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_gender.name as gender_name'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_interaction.name as interaction_name'
                    ,'mst_customer_address.address as address'
                    ,'mst_customer_address.city_id as city_id'
                    ,'mst_customer_address.district as district'
                    ,'mst_customer_address.province_id as province_id'
                    ,'mst_customer_address.postal_code as postal_code'
                  )
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_interaction','mst_interaction.id','=', $table->getTable().'.interaction_id')
                  ->where($table->getTable().'.sales_id' , sess_user('id'));
        if($request->from_date != '' && $request->to_date != ''){
          $result->where($table->getTable().'.created_at' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.created_at' ,'<=' , $request->to_date);
        }

        $result->orderBy($table->getTable().'.created_at','DESC');
        $result->orderBy($table->getTable().'.status','ASC');
        $result->orderBy($table->getTable().'.lock_status','ASC');
        $result->orderBy($table->getTable().'.interaction_id','DESC');

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('full_name', function($module) {
              return $module->gender_name." ".$module->full_name;
          })
          ->addColumn('active', function($module) {
              $created =  "created: ".date('d-m-Y H:i',strtotime($module->created_at))."<br/>";
              $status =  $module->status ?  '<span class="label font-weight-bold label-lg  label-light-warning label-inline">Transaksi</span>' : '<span class="label font-weight-bold label-lg  label-light-info label-inline">Kunjungan</span>';
              $newold = $module->followup_status ? '<span class="label font-weight-bold label-lg  label-light-danger label-inline">Lama</span>' : '<span class="label font-weight-bold label-lg  label-light-info label-inline">Baru</span>';
              return '<center>'.$created.$newold."&nbsp".$status.'</center>';
          })
          ->addColumn('action', function($module) {
              $data_id ="'".$module->id."'";
              $edit = '<a href="#edithost" onclick="show_data(' .$data_id. ')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
          							    <span class="svg-icon svg-icon-md svg-icon-primary">
          							        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          							            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          							                <rect x="0" y="0" width="24" height="24"/>
          							                <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
          							                <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
          							            </g>
          							        </svg>
          							    </span>
          							</a>';
              $delete = '<a data-href="' . route('product.delete',['followup',$module->id]) . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
          							    <span class="svg-icon svg-icon-md svg-icon-danger">
          							        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          							            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          							                <rect x="0" y="0" width="24" height="24"/>
          							                <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
          							                <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
          							            </g>
          							        </svg>
          							    </span>
          							</a>';
              if($module->closing_status){
                return '<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> Data Closed</span>';
              }else{
                if($module->lock_status){
                  return '<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> Data Lock</span>';
                }else{
                  if($module->status){
                    return $edit;
                  }else{
                    return $edit . ' ' . $delete;
                  }
                }
              }
          })
          ->rawColumns(['active', 'action'])
          ->make(true);
    }

     public static function execute($request, $data = null) {
        static::init();
        if (is_null($data)) {
            $data = new Followup;
            $data->author = sess_user('name');
            $data->sales_id = sess_user('id');
            $data->created_by = sess_user('id');
            $data->created_at = currDate();
            // $data->shift_work_id = sess_shift('id');
        }else{
            $data->updated_by = sess_user('id');
            $data->updated_at = currDate();
        }

        if ($request->company_id){
          $data->company_id = $request->company_id;
        }else{
          $data->company_id = sess_user('company_id');
        }
        if ($request->customer_id){
          $data->customer_id = $request->customer_id;
        }else {
          $data->customer_id = strtoupper(generadeCode("Master","Customer",null, "GDF".date("ymd", time()), $numb=3));
        }
        if ($request->customer_address_id){
          $data->customer_address_id = $request->customer_address_id;
        }else {
          if ($request->address){
            $data->customer_address_id = strtoupper(generadeCode("Master","CustomerAddress",$data->customer_id, null, $numb=3));;
          }
        }

        if ($request->id) {
            $data->id = strtoupper($request->id);
        }else{
            $data->id = strtoupper(generadeCode("Product","Visitor",$data->company_id, $data->customer_id, $numb=3));
        }

        if ($request->shift_work_id){
          $data->shift_work_id = $request->shift_work_id;
        }
        if ($request->item_id){
          $data->item_id = $request->item_id;
        }
        if ($request->advertise_id){
          $data->advertise_id = $request->advertise_id;
        }
        if ($request->advertiser_name){
          $data->advertiser_name = $request->advertiser_name;
        }
        if ($request->interaction_id){
          $data->interaction_id = $request->interaction_id;
        }
        if ($request->quantity){
          $data->quantity = $request->quantity;
        }
        if ($request->price){
          $data->price = $request->price;
        }
        if ($request->courier_cost != null || $request->courier_cost != ""){
          $data->courier_cost = $request->courier_cost;
        }
        if ($request->insurance != null || $request->insurance != ""){
          $data->insurance = $request->insurance;
        }
        if ($request->cost_handler != null || $request->cost_handler != ""){
          $data->cost_handler = $request->cost_handler;
        }
        if ($request->voucher_total){
          $data->voucher_total = $request->voucher_total;
        }
        if ($request->transaction){
          $data->transaction = $request->transaction;
        }
        if ($request->voucher_id){
          $data->voucher_id = $request->voucher_id;
        }
        if ($request->market_id){
          $data->market_id = $request->market_id;
        }
        if ($request->courier_id){
          $data->courier_id = $request->courier_id;
        }
        if ($request->payment_type_id){
          $data->payment_type_id = $request->payment_type_id;
        }
        if ($request->bank_id){
          $data->bank_id = $request->bank_id;
        }
        if ($request->transaction_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->transaction_date)){
            $data->transaction_date = Carbon::createFromFormat('d-m-Y H:i', $request->transaction_date)->format('Y-m-d H:i');
          }else{
            $data->transaction_date = $request->transaction_date;
          }
        }
        if ($request->packing_id){
          $data->packing_id = $request->packing_id;
        }
        if ($request->packing_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->packing_date)){
            $data->packing_date = Carbon::createFromFormat('d-m-Y H:i', $request->packing_date)->format('Y-m-d H:i');
          }else{
              $data->packing_date = $request->packing_date;
          }
        }
        if ($request->delivery_no){
          $data->delivery_no = $request->delivery_no;
        }
        if ($request->delivery_refno){
          $data->delivery_refno = $request->delivery_refno;
        }
        if ($request->delivery_remark){
          $data->delivery_remark = $request->delivery_remark;
        }
        if ($request->delivery_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->delivery_date)){
            $data->delivery_date = Carbon::createFromFormat('d-m-Y H:i', $request->delivery_date)->format('Y-m-d H:i');
          }else{
            $data->delivery_date = $request->delivery_date;
          }
        }
        if ($request->confirm_status){
          $data->confirm_status = $request->confirm_status;
        }
        if ($request->confirm_by){
          $data->confirm_by = $request->confirm_by;
        }
        if ($request->confirm_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->confirm_date)){
            $data->confirm_date = Carbon::createFromFormat('d-m-Y H:i', $request->confirm_date)->format('Y-m-d H:i');
          }else{
            $data->confirm_date = $request->confirm_date;
          }
        }
        if ($request->closing_status){
          $data->closing_status = $request->closing_status;
        }
        if ($request->closing_by){
          $data->closing_by = $request->closing_by;
        }
        if ($request->closing_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->closing_date)){
            $data->closing_date = Carbon::createFromFormat('d-m-Y H:i', $request->closing_date)->format('Y-m-d H:i');
          }else{
            $data->closing_date = $request->closing_date;
          }
        }
        if ($request->img_transaction){
          $data->img_transaction = $request->img_transaction;
        }
        if ($request->img_packing){
          $data->img_packing = $request->img_packing;
        }
        if ($request->img_delivery){
          $data->img_delivery = $request->img_delivery;
        }
        if ($request->followup_status){
          $data->followup_status = $request->followup_status;
        }
        if ($request->except("status")) {
            $data->status = to_bool($request->status);
        }
        $data->followup_status = 1;
        $data->save();
        return $data;
    }

}
