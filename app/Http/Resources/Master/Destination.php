<?php

namespace App\Http\Resources\Master;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_destination';
}
