<?php

namespace App\Http\Resources\Master;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_voucher';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
     protected $primaryKey = 'id';
     public $incrementing = false;
     protected $keyType = 'string';
}
